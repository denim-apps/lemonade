const caches = {};

export const invalidate = (cacheKey, id) => {
  if (caches[cacheKey]?.cache[id]) {
    delete caches[cacheKey]?.cache[id];
  }
};

/**
 * 
 * @param {string} cacheKey 
 * @param {{
 *  retrieve: (id: string) => Promise<any>,
 *  pollTime: number,
 *  expiry: number,
 * }} param1 
 */
export const createCacher = (cacheKey, {
  retrieve = async (id) => ({}),
  pollTime = 10000,
  expiry = 120000,
}) => {
  /**
   * No better cache than in-memory, right? This is a STUB.
   */
  if (!caches[cacheKey]) {
    caches[cacheKey] = {
      cache: {},
      expiryTimes: {},
    };
  }

  const { cache, expiryTimes } = caches[cacheKey];

  return async (id) => {
    // Disable caching for development mode.
    if (process.env.DISABLE_CACHE || (process.env.NODE_ENV !== 'production' && !process.env.FORCE_CACHE)) {
      return await retrieve(id);
    }

    if (!cache[id]) {
      cache[id] = await retrieve(id);
    }

    if (!expiryTimes[id]) {
      expiryTimes[id] = Date.now() + expiry;

      // Start polling.
      const refresh = async () => {
        const start = Date.now();
        cache[id] = await retrieve(id);

        if (Date.now() < expiryTimes[id]) {
          const timeLeft = pollTime - (Date.now() - start);

          // Queue the next one.
          if (timeLeft > 0) {
            setTimeout(refresh, timeLeft);
          } else {
            setImmediate(refresh);
          }
        } else {
          // Clear it from the cache.
          delete expiryTimes[id];
          delete cache[id];
        }
      };

      setTimeout(refresh, pollTime);
    }

    expiryTimes[id] = Date.now() + expiry;

    return cache[id];
  };
};
