import moment from 'moment';
import PaydayClient from '../../../connections/payday';
import { listRecords, updateRecords } from '../../../denim/airtable/data';
import { quoteValue } from '../../../denim/airtable/schema';
import RecordPayslip from './record';
import LarkApi from '../../../denim/lark/api';
import syncLog from '../sync/sync-log';
import upsertRecord from '../../../denim/utils/upsert-record';

const sendPayslip = async (payslip, openId, baseUrl) => {
  LarkApi.sendMessage(openId, {
    post: {
      en_us: {
        title: `Your payslip is here! 🎉`,
        content: [
          [
            {
              "tag": "text",
              "text": "Hello! Your payslip is now available for viewing and downloading. You may click the attachment below to access the file.",
            },
          ],
          [],
          [
            {
              "tag": "text",
              "text": "If you have any questions regarding your payroll and payslip, please reach out to your HR Team.",
            },
          ],
          [],
          [
            {
              "tag": "text",
              "text": "Thank you and stay safe! 👋🏻",
            },
          ],
          [],
          [
            {
              "tag": "text",
              "text": "Lemonade Team 🍋",
            },
          ],
          [],
          [
            {
              "tag": "a",
              "text": "View Payslip",
              "href": baseUrl + `/api/download-payslip/${payslip.id}`
            }
          ],
        ],
      },
    },
  });
};

/**
 * @param {any} options 
 * @param {async (data: any) => void} updateState 
 */
const payslip = async (options, updateState) => {
  if (options.type === 'generate') {
    updateState({
      type: 'generate',
      options,
      step: 0,
      stepText: 'Retrieving employees...',
      totalSteps: 1,
    });

    // Retrieve employee IDs within this pay period.
    const payrollPeriod = await PaydayClient.getPayrollPeriod(options.payrollPeriod);
    const groupId = payrollPeriod.Grouping.Id;
    const { records: airtableEmployees } = await listRecords('Employee', {
      fields: ['Employee ID', 'PDY ID'],
      filterByFormula: `{Payroll Group ID}=${quoteValue(String(groupId))}`,
      all: true,
      ids: options.employees?.length ? options.employees : null,
      expand: ['Pay Basis'],
    });

    const failures = [];
    const generated = [];
    const empties = [];
    let hasEmpty = false;
    let hasData = false;

    updateState({
      options,
      step: 0,
      stepText: 'Processing...',
      totalSteps: airtableEmployees.length,
      failures,
    });

    for (let i = 0; i < airtableEmployees.length; i++) {
      const employee = airtableEmployees[i];
      const employeeId = employee.fields['Employee ID'];

      updateState({
        options,
        step: i,
        stepText: `Processing ${employeeId}...`,
        totalSteps: airtableEmployees.length,
      });

      if (employeeId) {
        try {
          const Id = employee.fields['PDY ID'];
          const payslips = await PaydayClient.generatePayslip(options.payrollPeriod, Id);
          hasData = hasData || payslips.length > 0;
          hasEmpty = hasEmpty || payslips.length === 0;

          if (!payslips.length) {
            empties.push(employeeId);
          }

          for (let i = 0; i < payslips.length; i++) {
            const payslip = payslips[i];
            const payslipRecord = RecordPayslip({ payslip, periodId: options.payrollPeriod });

            generated.push({
              fields: payslipRecord,
            });
          }
        } catch (e) {
          if (e.loggable) {
            let response = await (e.inner?.text?.() || 'n/a');
            const log = `${e.message}\nENDPOINT: ${e.endpoint
              }\n\nHEADERS:\n${Object.keys(e.headers)
                .map((header) => `${header}: ${e.headers[header]}`)
                .join('\n')}\n\nDATA: ${JSON.stringify(
                  e.data
                )}\n\nRESPONSE: ${response}`;

            console.log(
              `[PAYSLIP] PAYSLIP GENERATION ERROR: ${employee.fields['Employee ID']}\n${log}`
            );

            await syncLog(employee.id, 'LEMONADE-PAYSLIP-GENERATION', log);
          }

          if (e.response?.body) {
            await syncLog(employee.id, 'LEMONADE-PAYSLIP-GENERATION', e.response?.body);
          }

          failures.push(`${employeeId} (Error: ${e.response?.body || e.message})`);
          console.error(e.response?.body || e);

          updateState({
            failures,
          });
        }
      }
    }

    console.log(hasData, hasEmpty);

    if (hasData) {
      if (hasEmpty) {
        // Partial failure.
      } else {
        /*
        await updateRecords('Payslip', generated, true);
        await upsertRecord('Payroll Periods', 'Payroll Period ID', {
          'Payroll Period ID': String(options.payrollPeriod),
          Generated: true,
        });
        */
      }
    } else {
      // Total failure.
    }

    updateState({
      options,
      step: airtableEmployees.length,
      stepText: `Generation complete.`,
      totalSteps: airtableEmployees.length,
      __final: true,
    });
  }

  if (options.type === 'release') {
    updateState({
      options,
      step: 0,
      stepText: `Releasing...`,
      totalSteps: 1,
    });
    const payrollPeriod = await PaydayClient.getPayrollPeriod(options.payrollPeriod);
    const { Start, End } = payrollPeriod || {};
    const periodName = `${moment(String(Start)).format(
      'MMM DD, Y',
    )}~${moment(String(End)).format(
      'MMM DD, Y',
    )}`;

    const { records: employees } = await listRecords('Employee', {
      all: true,
      fields: ['Employee ID', 'Lark ID'],
    });

    const { records: payslips } = await listRecords('Payslip', {
      all: true,
      filterByFormula: `{PayrollPeriod} = ${quoteValue(periodName)}`,
    });

    updateState({
      options,
      step: 0,
      stepText: `Releasing...`,
      totalSteps: payslips.length,
    });

    for (let i = 0; i < payslips.length; i++) {
      const payslip = payslips[i];
      updateState({
        options,
        step: i + 1,
        stepText: `Releasing to ${payslip.fields['Employee Id']}...`,
        totalSteps: 1,
      });

      const employee = employees.find((employee) => employee.fields['Employee ID'] === payslip.fields['Employee Id']);

      if (employee && employee.fields['Lark ID']) {
        await sendPayslip(payslip, employee.fields['Lark ID'], options.baseUrl);
      }
    }

    await upsertRecord('Payroll Periods', 'Payroll Period ID', {
      'Payroll Period ID': String(options.payrollPeriod),
      Released: true,
    });

    updateState({
      step: payslips.length,
      totalSteps: payslips.length,
      stepText: `Release complete.`,
      __final: true,
    })
  }
};

export default payslip;
