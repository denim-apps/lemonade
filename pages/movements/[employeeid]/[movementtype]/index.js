import { Button, Col, Divider, Row, Select, Space, Table } from 'antd';
import { SubmitButton } from 'formik-antd';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useState } from 'react';
import admin from '../../../../components/admin';
import AppLayout from '../../../../components/layout';
import LemonadeIcon from '../../../../components/lemonade-icon';
import { movementTypes } from '../../../../config/validation-extensions';
import { listRecords, retrieveRecord } from '../../../../denim/airtable/data';
import AirTableFormHelpers from '../../../../denim/airtable/form-helpers';
import { findTableSchema, quoteValue } from '../../../../denim/airtable/schema';
import AirTableField from '../../../../denim/components/AirTableField';
import AirTableForm from '../../../../denim/components/AirTableForm';
import { useAirTableCollection } from '../../../../denim/components/useAirTableCollection';

function getFetchOptions(employeeId, movementType) {
  return {
    all: true,
    filterByFormula: `{Employee ID}=${quoteValue(employeeId)}`,
    expand: findTableSchema(movementType.table)
      .columns
      .filter(({ type }) => type === 'foreignKey')
      .map(({ name }) => name),
    sort: [
      {
        field: 'Effective Date',
        direction: 'desc',
      },
    ],
  };
};

export const getServerSideProps = admin(async (ctx) => {
  if (!movementTypes[ctx.query.movementtype]) {
    return {
      notFound: true,
    };
  }

  const movementType = movementTypes[ctx.query.movementtype];

  const employee = await retrieveRecord('Employee', ctx.query.employeeid, {
    user: ctx.user,
  });

  if (!employee) {
    return {
      notFound: true,
    };
  }

  const { records: [employee201] } = await listRecords('201 Management', {
    filterByFormula: `{Employee ID}=${quoteValue(employee.fields['Employee ID'])}`,
    user: ctx.user,
  });

  if (!employee201) {
    return {
      notFound: true,
    };
  }

  const movements = await listRecords(movementType.table, {
    ...getFetchOptions(employee.fields['Employee ID'], movementType),
    user: ctx.user,
  });

  return {
    props: {
      movementType: {
        id: ctx.query.movementtype,
        ...movementType,
      },
      movements: {
        ...movements,
        offset: movements.offset || null,
      },
      employeeId: employee.fields['Employee ID'],
      employee,
      employee201,
    },
  };
});

const Movements = ({
  movementType,
  movements,
  employee,
  employee201,
  employeeId,
}) => {
  const {
    records,
    columns,
    isRetrieving,
    refresh,
  } = useAirTableCollection(
    movementType.table,
    useMemo(() => [
      'Employee ID',
      'Full Name',
      'Effective Date',
      ...movementType.columns,
    ], [movementType]),
    useMemo(() => getFetchOptions(employeeId, movementType), []),
    movements,
  );
  const [editingRecord, setEditingRecord] = useState(null);
  const router = useRouter();

  useEffect(() => {
    setEditingRecord(null);
  }, [records]);

  return (
    <AppLayout
      menuItemKey="employees"
      breadcrumb={[
        {
          label: 'Employees',
          key: 'employees',
          href: '/employees',
        },
        {
          label: employee.fields['Full Name'] || employeeId,
          key: 'employee',
          href: `/employees/${employee.id}`,
        },
        {
          label: 'Movements',
          key: 'movements',
          href: `/movements/${employee.id}/job-position`,
        },
        {
          label: movementType.label,
          key: 'movement',
          href: `/movements/${employee.id}/job-position/${movementType.id}`,
        },
      ]}
    >
      <Row gutter={24} style={{ marginBottom: 16 }}>
        <Col flex="auto">
          <Select
            style={{ width: '100%' }}
            value={movementType.id}
            onChange={(value) => {
              router.push(`/movements/${employee.id}/${value}`);
            }}
          >
            {Object.keys(movementTypes).map((type) => {
              const movementType = movementTypes[type];

              return (
                <Select.Option key={type}>
                  {movementType.label}
                </Select.Option>
              );
            })}
          </Select>
        </Col>
      </Row>
      <Table
        columns={
          useMemo(() => [
            ...columns,
            {
              title: '#',
              key: 'action',
              render: (text, record) => (
                <Space size="middle">
                  <a
                    href="/#"
                    onClick={(e) => {
                      e.stopPropagation();
                      e.preventDefault();
                      setEditingRecord(record.originalRecord);
                    }}
                  >
                    <LemonadeIcon type="pencil" />
                  </a>
                  <a
                    href="/#"
                    onClick={(e) => {
                      e.stopPropagation();
                      e.preventDefault();

                      AirTableFormHelpers.deleteRecord(movementType.table, record.id, {
                        postDelete: () => refresh(),
                      });
                    }}
                  >
                    <LemonadeIcon type="delete" />
                  </a>
                </Space>
              ),
            },
          ], [columns])
        }
        dataSource={records}
        pagination={false}
        loading={isRetrieving}
      />
      {editingRecord ? (
        <>
          <Divider orientation="left">Edit {movementType.label} Movement</Divider>
          <AirTableForm
            table={movementType.table}
            existingRecord={editingRecord}
            layout="inline"
            onSubmit={AirTableFormHelpers.submitUpdateHandler(movementType.table, {
              currentRecord: editingRecord,
              callback: () => {
                setEditingRecord(null);
                refresh();
              },
            })}
            key={editingRecord.id}
          >
            <Row gutter={24} style={{ width: '100%' }}>
              <Col flex={1}>
                <AirTableField columnName="Effective Date" style={{ width: '100%' }} />
              </Col>
              {movementType.columns.map((column) => (
                <Col flex={1} key={column}>
                  <AirTableField columnName={column} style={{ width: '100%' }} />
                </Col>
              ))}
              <Col>
                <Space>
                  <SubmitButton>
                    Save
                  </SubmitButton>
                  <Button onClick={() => setEditingRecord(null)}>
                    Cancel
                  </Button>
                </Space>
              </Col>
            </Row>
          </AirTableForm>
        </>
      ) : null}
      <Divider orientation="left">Add New {movementType.label} Movement</Divider>
      <AirTableForm
        table={movementType.table}
        initialValues={{
          'User ID': [employee201.id],
        }}
        layout="inline"
        onSubmit={(values, actions) => AirTableFormHelpers.submitCreateHandler(movementType.table, {
          callback: (record) => {
            actions.resetForm();
            refresh();
          },
        })(values, actions)}
      >
        <Row gutter={24} style={{ width: '100%' }}>
          <Col flex={1}>
            <AirTableField columnName="Effective Date" style={{ width: '100%' }} />
          </Col>
          {movementType.columns.map((column) => (
            <Col flex={1} key={column}>
              <AirTableField columnName={column} style={{ width: '100%' }} />
            </Col>
          ))}
          <Col>
            <SubmitButton>
              Add
            </SubmitButton>
          </Col>
        </Row>
      </AirTableForm>
    </AppLayout>
  );
};

export default Movements;
