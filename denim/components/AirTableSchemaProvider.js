import React, { createContext, useContext, useMemo } from 'react';
import { findTableSchema } from '../airtable/schema';
import { createValidationShapeFromSchema } from '../airtable/validation';

/**
 * @type {React.Context<{
 *  tableSchema: import('../airtable/schema-retriever').AirTable,
 *  shape: { [key: string]: import('yup').BaseSchema },
 *  existingRecord: import('../airtable/data').AirTableRecord,
 * }>}
 */
const AirTableSchemaContext = createContext({
  tableSchema: null,
  shape: {},
});

export const useAirTableSchema = () => useContext(AirTableSchemaContext);

/**
 * @type {React.FunctionComponent<{
 *  table: string,
 *  existingRecord: import('../airtable/data').AirTableRecord,
 * } & import('formik').FormikConfig>}
 */
const AirTableSchemaProvider = ({
  table,
  existingRecord,
  children,
}) => {
  const tableSchema = useMemo(() => {
    return findTableSchema(table);
  }, [table]);

  const validationShape = useMemo(() => {
    if (tableSchema) {
      return createValidationShapeFromSchema(tableSchema);
    }

    return null;
  }, [tableSchema]);

  if (tableSchema) {
    return (
      <AirTableSchemaContext.Provider
        value={{
          tableSchema,
          shape: validationShape,
          existingRecord,
        }}
      >
        {children}
      </AirTableSchemaContext.Provider>
    );
  }

  return null;
};

export default AirTableSchemaProvider;
