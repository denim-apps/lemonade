const moduleRequire = require('esm')(module);

module.exports = {
  jobs: moduleRequire('./config/jobs.js').default,
};
