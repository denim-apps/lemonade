import got from 'got';
import { listRecords, updateRecords } from '../../../denim/airtable/data';
import LarkApi from '../../../denim/lark/api';
import diffValues from '../../../denim/utils/diff-values';
import syncLog from './sync-log';
import createSyncer from './syncer';

const employeesSync = createSyncer({
  name: 'User',
  init: async (options, context) => {
    context.accessToken = await LarkApi.getAppAccessToken();
  },
  destinationRetriever: async (options, context) => {
    console.log('[LARK EMPLOYEE SYNC] Retrieving users from Lark...');
    const larkUsers = [];
    const allDepartments = [{ department_id: 0 }, ...options.allDepartments];

    for (let i = 0; i < allDepartments.length; i++) {
      const department = allDepartments[i];
      let pageToken = -1;

      while (pageToken) {
        const usersPage = await got(`https://open.larksuite.com/open-apis/contact/v3/users?department_id=${department.department_id}&department_id_type=department_id&${pageToken !== -1 ? `&page_token=${pageToken}` : ''}`, {
          headers: {
            Authorization: 'Bearer ' + context.accessToken,
          },
        }).json();

        larkUsers.push(...(usersPage.data.items || []));

        pageToken = usersPage.data.page_token;
      }
    }

    return larkUsers;
  },
  sourceRetriever: async (options, context) => {
    console.log('[LARK EMPLOYEE SYNC] Retrieving users from AirTable...');

    const { records } = await listRecords('Employee', {
      all: true,
      expand: ['Department'],
    });

    return records;
  },
  sourceItemValid: (airtableUser) => (airtableUser.fields['Email'] || (airtableUser.fields['Mobile Number']))
    && airtableUser.fields['Department']?.length
    && (!airtableUser.fields['Mobile Number'] || /^\+\d+$/g.exec(airtableUser.fields['Mobile Number'])),
  findInDestination: (larkUsers, airtableUser) => {
    let larkUser = null;

    // Find the user by email/mobile/employee ID.
    if (!larkUser) {
      larkUser = larkUsers.find(
        ({ email, mobile, employee_no }) => (email && airtableUser.fields['Email'] && email === airtableUser.fields['Email'])
          || (mobile && airtableUser.fields['Mobile Number'] && mobile === airtableUser.fields['Mobile Number'])
          || (employee_no && airtableUser.fields['Employee ID'] && employee_no === airtableUser.fields['Employee ID'])
      );
    }

    // Fallback to finding the user by ID.
    if (!larkUser) {
      // Check if this user exists in Lark.
      const userById = larkUsers.find(({ open_id }) => open_id === airtableUser.fields['Lark ID']);

      if (userById) {
        larkUser = userById;
      }
    }

    return larkUser;
  },
  convertSourceToDestination: (airtableUser) => {
    const converted = {
      name: airtableUser.fields['Full Name'],
      employee_no: airtableUser.fields['Employee ID'],
      mobile: airtableUser.fields['Mobile Number'],
      email: airtableUser.fields['Email'],
      city: airtableUser.fields['City'],
      country: airtableUser.fields['Country'],
      department_ids: airtableUser
        .children['Department']
        ?.map((department) => department.fields['Lark ID'])
        || [],
    };


    if (airtableUser.fields['Gender'] === 'Male') {
      converted.gender = 1;
    }

    if (airtableUser.fields['Gender'] === 'Female') {
      converted.gender = 2;
    }

    return converted;
  },
  requiresUpdate: (convertedSourceItem, destinationItem) => {
    const difference = diffValues(destinationItem, convertedSourceItem, true);

    return Object.keys(difference).length > 0;
  },
  destinationUpdator: async (convertedSourceItem, destinationItem, airtableUser, options, context) => {
    const difference = diffValues(destinationItem, convertedSourceItem, true);
    console.log('[LARK EMPLOYEE SYNC] Updating lark user ' + airtableUser.fields['User ID'], difference);

    // Update the existing user.
    try {
      const response = await got(`https://open.larksuite.com/open-apis/contact/v3/users/${destinationItem.open_id}?user_id_type=open_id&department_id_type=department_id`, {
        method: 'PATCH',
        headers: {
          Authorization: 'Bearer ' + context.accessToken,
        },
        json: difference,
      }).json();

      if (response.data.user) {
        console.log('[LARK EMPLOYEE SYNC] Updated Lark user ' + response.data.user.open_id);

        return response.data.user;
      }
    } catch (e) {
      await syncLog(airtableUser.id, 'EMPLOYEE-SYNC', `
UPDATE ERROR ${JSON.stringify(difference, null, '  ')}

ERROR:
${e.response?.body || e.message}

CONVERTED SOURCE ITEM:
${JSON.stringify(convertedSourceItem, null, '  ')}

DESTINATION ITEM:
${JSON.stringify(destinationItem, null, '  ')}

SOURCE ITEM:
${JSON.stringify(airtableUser, null, '  ')}
      `.trim());
      console.log('[LARK EMPLOYEE SYNC] Error updating ' + airtableUser.fields['User ID'] + ' ' + (e.response?.body || e.message));
    }
  },
  destinationCreator: async (convertedSourceItem, airtableUser, options, context) => {
    try {
      console.log('[LARK EMPLOYEE SYNC] Creating lark user ' + airtableUser.fields['User ID']);

      // Create a new user.
      const response = await got(`https://open.larksuite.com/open-apis/contact/v3/users?department_id_type=department_id`, {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + context.accessToken,
        },
        json: {
          ...convertedSourceItem,
          employee_type: 1,
          need_send_notification: true,
        },
      }).json();

      if (response.data.user) {
        console.log('[LARK EMPLOYEE SYNC] Created Lark user ' + response.data.user.open_id);
        return response.data.user;
      }
    } catch (e) {
      await syncLog(airtableUser.id, 'EMPLOYEE-SYNC', `
CREATION ERROR ${e.response?.body || e.message}

PAYLOAD:
${JSON.stringify({
  ...convertedSourceItem,
  employee_type: 1,
  need_send_notification: true,
}, null, '  ')}

CONVERTED SOURCE ITEM:
${JSON.stringify(convertedSourceItem, null, '  ')}

SOURCE ITEM:
${JSON.stringify(airtableUser, null, '  ')}
      `.trim());
      console.log('[LARK EMPLOYEE SYNC] Error creating ' + airtableUser.fields['User ID'] + ' ' + (e.response?.body || e.message));
    }
  },
  associate: async (airtableUser, larkUser) => {
    if (larkUser) {
      if (larkUser.open_id !== airtableUser.fields['Lark ID']) {
        await updateRecords('Employee', {
          id: airtableUser.id,
          fields: {
            'Lark ID': larkUser.open_id,
          },
        });

        if (airtableUser.fields['Lark ID']) {
          console.warn('[LARK EMPLOYEE SYNC] Warning: Open ID for ' + airtableUser.fields['User ID'] + ' changed from ' + airtableUser.fields['Lark ID'] + ' to ' + larkUser.open_id);
        }
      }
    }
  },
});

export default employeesSync;
