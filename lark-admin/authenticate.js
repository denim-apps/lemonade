const larkAdminApi = require('./admin-api');

larkAdminApi.init().then(() => {
  console.log('Lark admin authenticated.');
});
