import withSession from '../../../../denim/session';

const handler = withSession(
  async (req, res) => {
    if (req.user) {
      // User is already authenticated. Why are we here?
      return res.redirect('/');
    }

    // Get access token.
    const redirectUrl = req.session.get('redirect_url');
    req.session.set('lark_user_id', req.query.id);
    req.session.set('auth_expiry', 0);
    await req.session.save();

    if (redirectUrl) {
      req.session.unset('redirect_url');
      return res.redirect(redirectUrl);
    }

    return res.redirect('/');
  }
);

export default handler;
