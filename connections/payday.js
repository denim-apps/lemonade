import got from 'got';

const credentials = {
  baseUrl: `${process.env.PAYDAY_BASE_URL || 'http://localhost:1347'}/api`,
  merchantId: process.env.PAYDAY_MERCHANT_ID || '',
  merchantKey: process.env.PAYDAY_MERCHANT_KEY || '',
};

const getHeaders = async () => {
  const key = await got(`${credentials.baseUrl}/auth/merchants/signin`, {
    method: 'POST',
    json: {
      merchant_id: credentials.merchantId,
      merchant_Key: credentials.merchantKey,
    },
  }).json();

  return {
    Authorization: `Bearer ${key}`,
  };
};

const PaydayClient = {
  getHeaders,
  getPayrollPeriods: async () => {
    return await got(`${credentials.baseUrl}/payroll/periods`, {
      headers: {
        ...(await getHeaders()),
      },
    }).json();
  },
  getPayrollPeriod: async (id) => {
    return await got(`${credentials.baseUrl}/payroll/periods/${id}`, {
      headers: {
        ...(await getHeaders()),
      },
    }).json();
  },
  getEmployees: async () => {
    return await got(`${credentials.baseUrl}/employees`, {
      headers: {
        ...(await getHeaders()),
      },
    }).json();
  },
  getEmployee: async (id) => {
    return await got(`${credentials.baseUrl}/employees/${id}`, {
      headers: {
        ...(await getHeaders()),
      },
    }).json();
  },
  updateEmployee: async (employeeId, payload) => {
    const headers = await getHeaders();

    try {
      return await got(`${credentials.baseUrl}/employees/${employeeId}`, {
        method: 'PUT',
        json: payload,
        headers,
      }).json();
    } catch (e) {
      throw prepareError('Error updating employee', 'PUT /employees/' + employeeId, headers, payload, e);
    }
  },
  createEmployee: async (payload) => {
    const headers = await getHeaders();

    try {
      return await got(`${credentials.baseUrl}/employees`, {
        method: 'POST',
        json: payload,
        headers,
      }).json();
    } catch (e) {
      throw prepareError('Error creating employee', 'POST /employees', headers, payload, e);
    }
  },
  retrieve: async (table) => {
    return await got(`${credentials.baseUrl}/${table}`, {
      headers: {
        ...(await getHeaders()),
      },
    }).json();
  },
  generatePayslip: async (payroll_period_id, employee_id) => {
    const headers = await getHeaders();

    try {
      return await got(`${credentials.baseUrl}/slip/payroll/period`, {
        method: 'POST',
        json: {
          payroll_period_id,
          employee_id,
        },
        headers,
      }).json();
    } catch (e) {
      throw prepareError('Error generating payslip', 'POST /slip/payroll/period', headers, {
        payroll_period_id,
        employee_id,
      }, e);
    }
  },
};

const prepareError = (message, endpoint, headers, data, e) => {
  const error = new Error(message);
  error.loggable = true;
  error.endpoint = endpoint;
  error.headers = headers;
  error.inner = e;
  error.data = data;

  return error;
};

export default PaydayClient;
