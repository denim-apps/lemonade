/**
 * Creates a syncer. A syncer works by retrieving all items from the source and the destination. It then compares them by looking for the source item in the destination
 * via findInDestination(). If the source item is valid (sourceItemValid) and the destination has the item, it attempts an update via destinationUpdator() if requiresUpdate()
 * returns true. If it doesn't find the item in the destination, it runs destinationCreator(). Both the creator and updator should return the updated record (or just an ID)
 * which will be passed to the associate() function.
 */
const createSyncer = ({
  init = async (options, context) => { },
  destinationRetriever = async (options, context) => ([]),
  sourceRetriever = async (options, context) => ([]),
  sourceItemValid = async (item, options, context) => true,
  findInDestination = async (destinationItems, sourceItem, options, context) => { },
  convertSourceToDestination = async (sourceItem, options, context) => ({ }),
  requiresUpdate = async (convertedSourceItem, destinationItem, options, context) => { },
  destinationUpdator = async (convertedSourceItem, destinationItem, sourceItem, options, context) => 'id',
  destinationCreator = async (convertedSourceItem, sourceItem, options, context) => 'id',
  associate = async (sourceItem, result, options, context) => { },
  name = 'Generic',
}) => async (options) => {
  const context = { };

  // Initialize.
  await init(options, context);

  // Retrieve all items from source and destination.
  const destinationItems = await destinationRetriever(options, context);
  const sourceItems = await sourceRetriever(options, context);

  for (let i = 0; i < sourceItems.length; i++) {
    const sourceItem = sourceItems[i];
    let destinationItem = null;

    try {
      // Ensure the source record is valid.
      if (await sourceItemValid(sourceItem, options, context)) {
        // Find the item in the destination.
        const foundDestinationItem = await findInDestination(destinationItems, sourceItem, options, context);

        if (foundDestinationItem) {
          destinationItem = foundDestinationItem;
        }

        // Convert the data from AirTable.
        const convertedSourceItem = await convertSourceToDestination(sourceItem, options, context);
        let result = null;

        if (destinationItem) {
          if (await requiresUpdate(convertedSourceItem, destinationItem, options, context)) {
            result = await destinationUpdator(convertedSourceItem, destinationItem, sourceItem, options, context);
          }
        } else {
          result = await destinationCreator(convertedSourceItem, sourceItem, options, context);
        }

        // Update the stored ID if necessary.
        await associate(sourceItem, result || destinationItem, options, context);
      }
    } catch (e) {
      console.error(name + ' sync failure', sourceItem, destinationItem, e.response?.body || e);
    }
  }
};

export default createSyncer;
