const { default: cheerio } = require('cheerio');
const { CookieJar } = require('tough-cookie');
const { default: got } = require('got');

/**
 * @typedef {'text'
 *   | 'multilineText'
 *   | 'phone'
 *   | 'select'
 *   | 'multiSelect'
 *   | 'checkbox'
 *   | 'date'
 *   | 'number'
 *   | 'rating'
 *   | 'formula'
 *   | 'foreignKey'
 *   | 'lookup'} AirTableColumnType
 * @global
 */

/**
 * @typedef {AirTableColumnDefinition<'foreignKey', AirTableForeignKeyTypeOptions>} AirTableForeignKeyColumn
 * @global
 */

/**
 * @typedef {AirTableColumnDefinition<'text', AirTableTextTypeOptions>
 *   | AirTableColumnDefinition<'multilineText', AirTableTextTypeOptions>
 *   | AirTableColumnDefinition<'phone', null>
 *   | AirTableColumnDefinition<'select', AirTableSelectTypeOptions>
 *   | AirTableColumnDefinition<'multiSelect', AirTableSelectTypeOptions>
 *   | AirTableColumnDefinition<'checkbox', AirTableCheckBoxTypeOptions>
 *   | AirTableColumnDefinition<'date', AirTableDateTypeOptions>
 *   | AirTableColumnDefinition<'number', AirTableNumberTypeOptions>
 *   | AirTableColumnDefinition<'rating', AirTableRatingTypeOptions>
 *   | AirTableColumnDefinition<'formula', AirTableFormulaTypeOptions>
 *   | AirTableColumnDefinition<'rollup', AirTableFormulaTypeOptions>
 *   | AirTableForeignKeyColumn
 *   | AirTableColumnDefinition<'lookup', AirTableLookupTypeOptions>} AirTableColumn
 * @global
 */

/**
 * @typedef {(ForeignTypeOptionsDefinition<'text'> & AirTableTextTypeOptions)
 *   | (ForeignTypeOptionsDefinition<'multilineText'> & AirTableTextTypeOptions)
 *   | (ForeignTypeOptionsDefinition<'phone'>)
 *   | (ForeignTypeOptionsDefinition<'select'> & AirTableSelectTypeOptions)
 *   | (ForeignTypeOptionsDefinition<'multiSelect'> & AirTableSelectTypeOptions)
 *   | (ForeignTypeOptionsDefinition<'checkbox'> & AirTableCheckBoxTypeOptions)
 *   | (ForeignTypeOptionsDefinition<'date'> & AirTableDateTypeOptions)
 *   | (ForeignTypeOptionsDefinition<'number'> & AirTableNumberTypeOptions)
 *   | (ForeignTypeOptionsDefinition<'rating'> & AirTableRatingTypeOptions)
 *   | (ForeignTypeOptionsDefinition<'foreignKey'> & AirTableForeignKeyTypeOptions)
 *   | (ForeignTypeOptionsDefinition<'lookup'> & AirTableLookupTypeOptions)} AirTableFormulaTypeOptions
 * @global
 */

/**
 * @typedef {{
 *   resultType: T;
 *   formulaTextParsed: string;
 *   dependencies?: {
 *     referencedColumnIdsForValue?: string[];
 *   };
 * }} ForeignTypeOptionsDefinition
 * @global
 */

/**
 * @typedef {Object} AirTable
 * @property {string} id
 * @property {string} name
 * @property {string} baseId
 * @property {AirTableColumn[]} columns 
 * @global
 */

/**
 * @typedef {Object} AirTableColumnDefinition
 * @property {string} id
 * @property {string} name
 * @property {T} type
 * @property {TO} [typeOptions] 
 * @template T, TO
 * @global
 */

/**
 * @typedef {Object} AirTableSelectTypeOptions
 * @property {{[x:string]:{id:string;name:string;color:string;};}} choices
 * @property {string[]} choiceOrder 
 * @global
 */

/**
 * @typedef {Object} AirTableCheckBoxTypeOptions
 * @property {string} color
 * @property {string} icon 
 * @global
 */

/**
 * @typedef {Object} AirTableDateTypeOptions
 * @property {boolean} isDateTime
 * @property {'Local'|'Friendly'|'US'|'European'|'ISO'} dateFormat
 * @property {'12hour'|'24hour'} [timeFormat]
 * @property {'client'|'UTC'} [timeZone] 
 * @global
 */

/**
 * @typedef {Object} AirTableTextTypeOptions
 * @property {'email'|'url'} validatorName 
 * @global
 */

/**
 * @typedef {Object} AirTableNumberTypeOptions
 * @property {'decimal'|'currency'|'percentV2'|'duration'} format
 * @property {boolean} [negative]
 * @property {'positive'} [validatorName]
 * @property {string} [durationFormat]
 * @property {string} [symbol]
 * @property {number} [precision] 
 * @global
 */

/**
 * @typedef {Object} AirTableRatingTypeOptions
 * @property {string} color
 * @property {string} icon
 * @property {number} max 
 * @global
 */

/**
 * @typedef {Object} AirTableForeignKeyTypeOptions
 * @property {string} foreignTableId
 * @property {'many'|'one'} relationship
 * @property {boolean} unreversed
 * @property {string} symmetricColumnId 
 * @global
 */

/** @typedef {Object} AirTableLookupTypeOptions
 * @property {string} relationColumnId
 * @property {string} foreignTableRollupColumnId
 * @property {Object} dependencies
 * @property {string[]} [dependencies.referencedColumnIdsForValue]
 * @property {AirTableColumnType} resultType 
 */
/**
 * @typedef {'text' | 'multilineText' | 'phone' | 'select' | 'multiSelect' | 'checkbox' | 'date' | 'number' | 'rating' | 'formula' | 'foreignKey' | 'lookup'} AirTableColumnType
 * @global
 */

/**
 * @typedef {Object} SchemaResult
 * @property {string} apiKey
 * @property {{
 *  [key: string]: {
 *    name: string;
 *    category: string;
*     id: string;
 *    tables: AirTable[];
 *  }
 * }} bases
 * @global
 */

/**
 * 
 * @param {string} email 
 * @param {string} password 
 * @param  {...string} baseIds 
 * @returns {Promise<SchemaResult>}
 */
const retrieveAirtableSchema = async (email, password, ...baseIds) => {
  const cookieJar = new CookieJar();

  const indexPage = await got('https://www.airtable.com/', {
    cookieJar,
  }).text();

  const loginPage = await got('https://airtable.com/login', {
    cookieJar,
  }).text();

  const loginPageSchema = cheerio.load(loginPage);
  const _csrf = loginPageSchema('[name=_csrf]').val();

  const loginAttempt = await got('https://airtable.com/auth/login/', {
    method: 'POST',
    form: {
      _csrf,
      email,
      password,
    },
    cookieJar,
    followRedirect: false,
  }).text();

  /**
   * @type {SchemaResult}
   */
  const result = {
    apiKey: '',
    bases: {},
  };

  if (loginAttempt === 'Found. Redirecting to /') {
    // Retrieve all bases.
    console.log('Retrieving bases...');
    const apiPage = await got('https://airtable.com/api', {
      cookieJar,
    }).text();
    const $apiPage = cheerio.load(apiPage);
    const baseIdList = [];

    $apiPage('a[href$="/api/docs"]').each(function () {
      const $link = $apiPage(this);
      const [, baseKey] = /\/(.+)\/api\/docs/g.exec(
        String($link.attr('href'))
      ) || [null, null];

      if (baseKey && (!baseIds.length || baseIds.includes(baseKey))) {
        baseIdList.push(baseKey);
        result.bases[baseKey] = {
          name: $link.text(),
          category: $link.parent().find('div.py1.mb1.quieter.strong').text(),
          id: baseKey,
          tables: [],
        };
      }
    });

    if (baseIds.length) {
      for (let i = 0; i < baseIdList.length; i++) {
        const baseId = baseIdList[i];

        // Success.
        const baseDocsPage = await got(
          `https://airtable.com/${baseId}/api/docs`,
          {
            cookieJar,
          }
        ).text();
        const baseDocsPageSchema = cheerio.load(baseDocsPage);

        if (!result.apiKey) {
          result.apiKey = String(
            baseDocsPageSchema('div[data-api-key]').attr('data-api-key')
          );
        }

        const window = {};
        eval(
          baseDocsPageSchema(baseDocsPageSchema('script')[1]).html() ||
          'window.empty = true;'
        );

        if (window.empty) {
          throw new Error(`Failed to retrieve schema for ${baseId}.`);
        }
        const tables = [];

        window.application.tables.forEach((table) => {
          tables.push({
            id: table.id,
            name: table.name,
            baseId,
            columns: table.columns.map((column) => ({
              id: column.id,
              name: column.name,
              type: column.type,
              typeOptions: column.typeOptions,
            })),
          });
        });

        result.bases[baseId].name = window.application.name;
        result.bases[baseId].tables = tables;
      }
    }

    return result;
  } else {
    // Get error.
    const loginPage = await got('https://www.airtable.com/login', {
      cookieJar,
    }).text();
    console.log(loginPage);
    throw new Error(cheerio.load(loginPage)('.small.strong.quiet').text());
  }
}

module.exports = { retrieveAirtableSchema };
