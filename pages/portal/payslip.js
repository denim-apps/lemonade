import { Button, Card, Divider, Progress, Result, Space, Spin, Tag } from 'antd';
import { BellOutlined, WarningOutlined } from '@ant-design/icons';
import Text from 'antd/lib/typography/Text';
import Title from 'antd/lib/typography/Title';
import { Formik, useField } from 'formik';
import { Form, Select, SubmitButton } from 'formik-antd';
import moment from 'moment';
import { useEffect, useState } from 'react';
import * as Yup from 'yup';
import admin from '../../components/admin';
import AppLayout from '../../components/layout';
import PaydayClient from '../../connections/payday';
import { findTableSchema, quoteValue } from '../../denim/airtable/schema';
import { AirTableLookupField } from '../../denim/components/AirTableField';
import JobQueue from '../../denim/job-queue';
import LemonadeLabel from '../../components/lemonade-label';
import { useAirTableCache } from '../../denim/components/AirTableCache';
import confirm from 'antd/lib/modal/confirm';
import { useRouter } from 'next/dist/client/router';
import { listRecords } from '../../denim/airtable/data';

export const getServerSideProps = admin(async (ctx) => {
  let existingJob = ctx.req.session.get('payslip_report_id') || null;

  if (existingJob && !(await JobQueue.jobExists(existingJob))) {
    existingJob = null;
  }

  return {
    props: {
      payrollPeriods: await PaydayClient.getPayrollPeriods(),
      atPayrollPeriods: (await listRecords('Payroll Periods', { all: true })).records,
      currentJobId: existingJob,
    },
  };
});

const SubmitButtons = ({
  setJobType,
  atPayrollPeriods,
}) => {
  const [{ value: periodId }] = useField('payrollPeriod');
  const [{ value: employeeIds }] = useField('employees');
  const payrollPeriod = atPayrollPeriods.find(({ fields = { } } = { }) => fields['Payroll Period ID'] == periodId)?.fields || { };

  return (
    <Space>
      <SubmitButton onClick={() => setJobType('generate')} disabled={!payrollPeriod || payrollPeriod.Released}>
        Generate
      </SubmitButton>
      <Button type="default" disabled={!payrollPeriod.Generated} href={`/api/download-payslip/${periodId}/${(employeeIds || []).join(',')}`} target="_blank">
        Download
      </Button>
      <SubmitButton type="default" onClick={() => setJobType('release')} disabled={!payrollPeriod.Generated || payrollPeriod.Released}>
        Release
      </SubmitButton>
    </Space>
  );
};

const PayslipPortal = ({
  payrollPeriods,
  currentJobId,
  atPayrollPeriods,
}) => {
  const router = useRouter();
  const [jobId, setJobId] = useState(currentJobId);
  const [jobData, setJobData] = useState(null);
  const [jobType, setJobType] = useState('generate');

  const updateJobStatus = async (id, longPoll) => {
    const response = await fetch('/api/jobs/payslip?id=' + id + '&longPoll=' + longPoll);
    const data = await response.json();
    setJobData(data);

    if (data.__final) {
      router.replace(router.asPath);
    } else {
      updateJobStatus(id, '1');
    }
  };

  useEffect(() => {
    if (jobId) {
      updateJobStatus(jobId, '');
    }
  }, [jobId]);

  return (
    <AppLayout
      title="Payslip Portal"
      breadcrumb={[
        {
          label: 'Payslip Portal',
          href: '/portal/payslip',
          key: 'payslip-portal',
        },
      ]}
      menuItemKey="payslip"
    >
      <Title>Payslip Generation Portal</Title>
      <Text>
        Generate and release payslips for employees for a pay period.
      </Text>
      <Divider />
      {jobId && !jobData ? (
        <>
          <Spin />
        </>
      ) : (
        <>
          <Formik
            initialValues={jobData?.options || {}}
            validationSchema={Yup.object().shape({
              payrollPeriod: Yup.string().required().label('Payroll Period'),
              employees: Yup.array(Yup.string()).nullable(true),
            })}
            onSubmit={async (values) => {
              if (jobType === 'release') {
                await new Promise((resolve, reject) => {
                  confirm({
                    title: 'Release Payslips',
                    icon: <BellOutlined />,
                    content: `You are about to release payslip to your selected employees.
                    Once this is released, re-processing is not allowed.
                    
                    Would you like to proceed?`,
                    okText: 'Yes',
                    okType: 'danger',
                    cancelText: 'No',
                    onOk: () => {
                      resolve();
                    },
                    onCancel: () => {
                      reject();
                    },
                  });
                });
              }

              const response = await fetch('/api/jobs/payslip', {
                method: 'POST',
                body: JSON.stringify({
                  ...values,
                  type: jobType,
                  baseUrl: window.location.origin,
                }),
                headers: {
                  'Content-Type': 'application/json',
                },
              });
              const { id } = await response.json();
              setJobId(id);
              setJobData(null);
            }}
          >
            <Form layout="vertical">
              <Form.Item name="payrollPeriod" required>
                <Select name="payrollPeriod" allowClear>
                  {payrollPeriods.filter(({ Id }) => !atPayrollPeriods.find(({ fields }) => fields['Payroll Period ID'] == Id && fields['Released'])).map(({ Id, Code, Desc, Start, End }) => (
                    <Select.Option key={Id}>
                      {Desc} ({Code})
                      &nbsp;
                      <Tag>
                        {moment(String(Start)).format(
                          'MMM DD, Y',
                        )}
                        &nbsp;-&nbsp;
                        {moment(String(End)).format(
                          'MMM DD, Y',
                        )}
                      </Tag>
                    </Select.Option>
                  ))}
                </Select>
                <LemonadeLabel required>Payroll Period</LemonadeLabel>
              </Form.Item>
              <EmployeeFilterField
                periods={payrollPeriods}
              />
              <SubmitButtons setJobType={setJobType} atPayrollPeriods={atPayrollPeriods} />
            </Form>
          </Formik>
          {jobData ? (
            jobData.type === 'generate' ? (
              <>
                <Divider />
                {jobData.failures?.length ? (
                  <Card title={<><WarningOutlined /> The following employee IDs failed to process</>} size="small" style={{ marginBottom: 16 }}>
                    {jobData.failures.map((user) => (
                      <Tag key={user} color="yellow">
                        {user}
                      </Tag>
                    ))}
                  </Card>
                ) : null}
                <div style={{ textAlign: 'center' }}>{jobData.stepText}</div>
                <Progress percent={jobData.step / jobData.totalSteps * 100} showInfo={false} />
                {jobData.errorMessage ? (
                  <Result status="error" title="Error" subTitle={jobData.errorMessage} />
                ) : null}
                {/*JSON.stringify(jobData)*/}
              </>
            ) : (
              <>
                <Divider />
                {jobData.failures?.length ? (
                  <Card title={<><WarningOutlined /> The following employee IDs failed to process</>} size="small" style={{ marginBottom: 16 }}>
                    {jobData.failures.map((user) => (
                      <Tag key={user} color="yellow">
                        {user}
                      </Tag>
                    ))}
                  </Card>
                ) : null}
                <div style={{ textAlign: 'center' }}>{jobData.stepText}</div>
                <Progress percent={jobData.step / jobData.totalSteps * 100} showInfo={false} />
                {jobData.errorMessage ? (
                  <Result status="error" title="Error" subTitle={jobData.errorMessage} />
                ) : null}
                {/*JSON.stringify(jobData)*/}
              </>
            )
          ) : null}
        </>
      )}
    </AppLayout>
  );
};

const EmployeeFilterField = () => null;

export default PayslipPortal;
