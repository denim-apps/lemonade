import { Button, Space, Table, Row, Col } from 'antd';
import Link from 'next/link';
import { useMemo } from 'react';
import admin from '../../components/admin';
import AppLayout from '../../components/layout';
import LemonadeIcon from '../../components/lemonade-icon';
import { listRecords } from '../../denim/airtable/data';
import AirTableFormHelpers from '../../denim/airtable/form-helpers';
import { useAirTableCollection } from '../../denim/components/useAirTableCollection';

const fetchOptions = {
  expand: ['Job Title', 'Account Status'],
  fields: [
    'Employee ID',
    'Last Name',
    'First Name',
    'Full Name',
    'Account Status',
    'Job Title',
  ],
};

export const getServerSideProps = admin(async (ctx) => {
  const { offset = null, records } = await listRecords('Employee', fetchOptions);

  return {
    props: {
      user: ctx.user,
      prefetch: {
        offset,
        records,
      },
    },
  };
});

const Employees = ({
  prefetch,
}) => {
  const { renderField: renderSearch, finalOptions } = AirTableFormHelpers.useSearchControl({
    table: 'Employee',
    formula: 'SEARCH(LOWER($search), LOWER(CONCATENATE({Employee ID}, {Account Status}, {Department}, {Job Title}, {Full Name}, {Marital Status}, {Gender}, {City}, {Barangay}, {State/Province}, {Company})))>0',
  }, fetchOptions);

  const {
    records,
    isRetrieving,
    retrieveMore,
    hasMore,
    columns,
    refresh,
  } = useAirTableCollection(
    'Employee',
    useMemo(() => [
      'Employee ID',
      'Last Name',
      'First Name',
      'Full Name',
      'Account Status',
      'Job Title',
    ], []),
    finalOptions,
    prefetch,
  );

  return (
    <AppLayout
      title="Employees"
      breadcrumb={[
        {
          label: 'Employees',
          href: '/employees',
          key: 'employees',
        }
      ]}
      menuItemKey="employees"
    >
      <div style={{ marginBottom: 16 }}>
        <Row gutter={24}>
          <Col flex="auto">
            {renderSearch()}
          </Col>
          <Col>
            <Link href="/employees/new">
              <Button type="primary" href="/employees/new">
                Create New Employee
              </Button>
            </Link>
          </Col>
        </Row>
      </div>
      <Table
        columns={[
          ...columns,
          {
            title: '#',
            key: 'action',
            render: (text, record) => (
              <Space size="middle">
                {record['Employee ID'] ? (
                  <Link href={`/movements/${record.id}/job-position`}>
                    <a title="View Employee Movements">
                      <LemonadeIcon type="list" />
                    </a>
                  </Link>
                ) : null}
                <Link href={`/employees/${record.id}`}>
                  <a>
                    <LemonadeIcon type="pencil" />
                  </a>
                </Link>
                <a
                  href="/#"
                  onClick={(e) => {
                    e.stopPropagation();
                    e.preventDefault();

                    AirTableFormHelpers.deleteRecord('Employee', record.id, {
                      title: 'Are you sure you want to delete this employee?',
                      description: 'This employee record will be permanently deleted. You must delete the Lark user manually in the Lark admin.',
                      postDelete: () => refresh(),
                    });
                  }}
                >
                  <LemonadeIcon type="delete" />
                </a>
              </Space>
            ),
          },
        ]}
        dataSource={records}
        pagination={false}
        loading={isRetrieving}
      />
      {hasMore ? (
        <div style={{ marginTop: 16, textAlign: 'right' }}>
          <Button onClick={retrieveMore} disabled={isRetrieving}>Load More</Button>
        </div>
      ) : null}
    </AppLayout>
  );
};

export default Employees;
