import withAuth, { requireAuth } from '../denim/auth';
import withSession from '../denim/session';

export const admin = (handler) => withSession(
  withAuth(
    requireAuth(
      async (ctx) => {
        if (!ctx.user?.employee?.fields['Is HR Admin'] && !ctx.user?.employee?.fields['Is HR User']) {
          return {
            notFound: true,
          };
        }
      
        const response = await handler(ctx);

        return {
          ...response,
          props: {
            ...(response.props || { }),
            user: ctx.user,
          },
        };
      },
    )
  )
);

export default admin;
