const RecordPayslip = ({ payslip }) => {
  let date = new Date();
  let options = { year: 'numeric', month: 'short', day: '2-digit' };
  let issuedDate = new Intl.DateTimeFormat('en-US', options).format(date);

  let createPayslip = {
    'Employee Id': payslip['EmpId'],
    'Name': payslip['Name'],
    'Location': payslip['Location'],
    'Department': payslip['Department'],
    'DateHired': payslip['DateHired'],
    'Monthly Rate': payslip.MonthlyRate,
    'Basic Rate': payslip.BasicRate,
    'YTDTaxInc': payslip.YTDTaxInc,
    'YTDWTax': payslip.YTDWTax,
    'Gross': payslip.Gross,
    'Total Deduction': payslip.TotalDeduction,
    'Net': payslip.Net,
    'PayrollPeriod': payslip['PayrollPeriod'],
    'Date Issued': issuedDate,
    'Income Breakdown': JSON.stringify(payslip.Incomes),
    'Deduction Breakdown': JSON.stringify(payslip.Deductions),
  };

  return createPayslip;
}

export default RecordPayslip;
