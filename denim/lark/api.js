import got from 'got';
import lark from '../../config/lark';

/**
 * @typedef {{
 *  avatar: {
 *    avatar_72: string,
 *    avatar_240: string,
 *    avatar_640: string,
 *    avatar_origin: string,
 *  },
 *  city: string,
 *  country: string,
 *  department_ids: string[],
 *  description: string,
 *  email: string,
 *  employee_no: string,
 *  employee_type: number,
 *  en_name: string,
 *  gender: number,
 *  is_tenant_manager: boolean,
 *  job_title: string,
 *  join_time: number,
 *  mobile_visible: boolean,
 *  name: string,
 *  open_id: string,
 *  orders: {
 *    department_id: string,
 *    department_order: number,
 *    user_order: number,
 *  }[],
 *  status: {
 *    is_activated: boolean,
 *    is_frozen: boolean,
 *    is_resigned: boolean,
 *  },
 *  union_id: string,
 *  user_id: string,
 *  work_station: string,
 * }} LarkUser
 * @global
 */

const LarkApi = {
  getAppAccessToken: async () => {
    const { app_access_token } = await got('https://open.larksuite.com/open-apis/auth/v3/app_access_token/internal', {
      method: 'POST',
      json: {
        app_id: lark.appId,
        app_secret: lark.appSecret,
      },
    }).json();

    return app_access_token;
  },
  sendMessage: async (open_id, content) => {
    const token = await LarkApi.getAppAccessToken();
    const response = await got('https://open.larksuite.com/open-apis/message/v4/send/', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      json: {
        open_id,
        msg_type: 'post',
        content,
      },
    });

    return response;
  },
};

export default LarkApi;
