import { retrieveRecord, updateRecords } from '../../../../denim/airtable/data';
import { findTableSchema } from '../../../../denim/airtable/schema';
import withAuth from '../../../../denim/auth';
import withSession from '../../../../denim/session';
import secureEndpoints from '../../../../config/secure-endpoints';

const handler = async (req, res) => {
  try {
    const { table, record } = req.query;
    const tableSchema = findTableSchema(table);

    if (tableSchema) {
      if (req.method === 'GET') {
        if (secureEndpoints[tableSchema.name]?.retrieve || process.env.DISABLE_OPEN_ENDPOINTS) {
          return res.status(404).end();
        }
  
        return res.json(await retrieveRecord(table, record, {
          ...req.query,
          user: req.user,
        }));
      }

      if (req.method === 'PATCH') {
        if (secureEndpoints[tableSchema.name]?.update || process.env.DISABLE_OPEN_ENDPOINTS) {
          return res.status(404).end();
        }
  
        return res.json(await updateRecords(table, {
          ...req.body,
          id: req.query.record,
        }, false, req.user));
      }
    }

    res.status(404).end();
  } catch (e) {
    if (e.isValidationError) {
      res.status(422).send({
        errors: e.validationErrors,
      });
      return;
    }

    if (e.response) {
      res.setHeader('Content-Type', 'application/json');
      res.status(e.response.statusCode).send(e.response.body);
    } else {
      throw e;
    }
  }
};

export default withSession(withAuth(handler));
