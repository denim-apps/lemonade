const absoluteOrigin = (
  req,
  localhostAddress = 'localhost:3000'
) => {
  if (typeof(window) !== 'undefined') {
    return window.location.origin;
  }

  let host = req?.headers.host || localhostAddress;

  let protocol = req.connection.encrypted ? 'https:' : 'http:';

  if (
    req &&
    req.headers['x-forwarded-host'] &&
    typeof req.headers['x-forwarded-host'] === 'string'
  ) {
    host = req.headers['x-forwarded-host'];
  }

  if (
    req &&
    req.headers['x-forwarded-proto'] &&
    typeof req.headers['x-forwarded-proto'] === 'string'
  ) {
    protocol = `${req.headers['x-forwarded-proto']}:`;
  }

  return protocol + '//' + host;
};

export default absoluteOrigin;
