import React, { createContext, useContext } from 'react';

/**
 * @type {React.Context<{
 *  user: import('../lark/api').LarkUser,
 * }>}
 */
const UserProviderContext = createContext({
  user: null,
});

export const useUser = () => useContext(UserProviderContext);

/**
 * @type {React.FunctionComponent<{ user: import('../lark/api').LarkUser }>}
 */
const UserProvider = ({
  user,
  children,
}) => {
  return (
    <UserProviderContext.Provider
      value={{ user }}
    >
      {children}
    </UserProviderContext.Provider>
  );
};

export default UserProvider;
