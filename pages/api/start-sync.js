import JobQueue from '../../denim/job-queue';

const handler = async (req, res) => {
  if (req.method === 'INTERNAL') {
    await JobQueue.enqueueJob('sync');
  }

  res.end();
};

export default handler;
