import { listRecords } from '../denim/airtable/data';
import { createCacher } from '../utils/cacher';

const retrieveUserRecord = createCacher('user-record', {
  retrieve: async (id) => {
    const { records: [record] } = await listRecords('Employee', {
      maxRecords: 1,
      filterByFormula: `{Lark ID}='${id}'`,
      fields: [
        'Employee ID',
        'Full Name',
        'Is HR Admin',
        'Is HR User',
        'Lark ID',
      ],
    });

    if (record) {
      return record;
    }

    return null;
  },
});

export const postUserRetrieval = async (req) => {
  if (req.user) {
    // Find the employee with this user account.
    const record = await retrieveUserRecord(req.user.open_id);

    if (record) {
      req.user.employee = record;
    }
  }
};
