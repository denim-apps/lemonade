import absoluteOrigin from '../../denim/absolute-origin';
import { listRecords } from '../../denim/airtable/data';
import { quoteValue } from '../../denim/airtable/schema';
import LarkApi from '../../denim/lark/api';

const sendPayslip = async (payslip, openId, baseUrl) => {
  LarkApi.sendMessage(openId, {
    post: {
      en_us: {
        title: `Your payslip is here! 🎉`,
        content: [
          [
            {
              "tag": "text",
              "text": "Hello! Your payslip is now available for viewing and downloading. You may click the attachment below to access the file.",
            },
          ],
          [],
          [
            {
              "tag": "text",
              "text": "If you have any questions regarding your payroll and payslip, please reach out to your HR Team.",
            },
          ],
          [],
          [
            {
              "tag": "text",
              "text": "Thank you and stay safe! 👋🏻",
            },
          ],
          [],
          [
            {
              "tag": "text",
              "text": "Lemonade Team 🍋",
            },
          ],
          [],
          [
            {
              "tag": "a",
              "text": "View Payslip",
              "href": baseUrl + `/api/download-payslip/${payslip.id}`
            }
          ],
        ],
      },
    },
  });
};

const handler = async (req, res) => {
  const periodName = 'Sep 26, 2021~Oct 10, 2021';

  const { records: employees } = await listRecords('Employee', {
    all: true,
    fields: ['Employee ID', 'Lark ID'],
  });
  const { records: payslips } = await listRecords('Payslip', {
    all: true,
    filterByFormula: `{PayrollPeriod} = ${quoteValue(periodName)}`,
  });

  const result = await sendPayslip(payslips[0], 'ou_6922b5577380d8360c5d8f5a4a280556', absoluteOrigin(req));
  console.log(result);

  res.send('OK');
};

export default handler;
