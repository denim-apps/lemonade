# DENIM Boilerplate

This is a boilerplate that contains everything you need to get started with creating applications with an AirTable backend, Next.js, and optionally Lark authentication. Begin by installing the dependencies with `yarn install`.

## Before You Begin

Before you can use DENIM effectively, I recommend you familiarize yourself with the components it uses. Here's some recommended reading:

- [AirTable](https://airtable.com/)
- [Next.js Documentation](https://nextjs.org/docs)
- [Ant Design Documentation](https://ant.design/docs/react/introduce)
- [Formik Documentation](https://formik.org/docs/overview)

## AirTable Schema

To kickstart the project, you'll have to retrieve schema from AirTable. This can be done via the "schema" command, for example `yarn schema init my@email.com mypassword appAAAAAAAAAAAAAA appAAAAAAAAAAAAAA`, where `appAAAAAAAAAAAAAA` is the ID of a base or bases. The base ID can be retrieved by opening the base's API [and looking here](https://i.imgur.com/RHQ0jxi.png). Run `yarn schema` for full usage documentation on the schema command. If you have changed your schema since last retrieval, you can run `yarn schema refresh` to refresh it from AirTable. If you need more bases to be connected to the app, just run `yarn schema init` with the full list of bases. `yarn schema delete` will delete the AirTable schema cache.

## Developing

To start developing the application, you can run `yarn dev`. DENIM is build on top of Next.js, so you can refer to its [documentation](https://nextjs.org/docs) for Next.js-specific questions.

## AirTable API Routes

The boilerplate sets up a layer on top of the AirTable API that is, for the mostpart, in sync with the actual API but with some additional features. For now, what you need to know is that we use the [slugify](https://www.npmjs.com/package/slugify) library to convert table names into URL slugs. You can also optionally edit `./config/table-aliases.js` to configure specific URL slugs if needed. So instead of accessing a table under https://api.airtable.com/v0/[base]/[table], you'd access it under /api/data/[table-slug] (e.x. https://api.airtable.com/v0/appAAAAAAAAAAAAAA/My%20Table becomes /api/data/my-table/). Additionally, this allows us to combine multiple bases under one application. In the case of multiple bases having the same table name, it uses the first base defined in the initial `yarn schema init` command.

Updates and creates have also been simplified. Instead of passing it under a records key like this:

```js
{
  "records": [
    {
      "fields": {
        "Name": "Test"
      }
    }
  ]
}
```

You can just pass them directly as an array or a single object:

```js
[
  {
    "fields": {
      "Name": "Test"
    }
  }
]

// or

{
  "fields": {
    "Name": "Test"
  }
}
```

### Additional Features

In addition to the usual AirTable API, DENIM introduces some additional features to speed up development & retrieval of data.

#### `expand`

The expand parameter can be specified as a query string parameter to all CRUD API calls. It allows you to expand the records under "Link to another record" fields. Under the usual AirTable API, these are returned as an array of IDs, however to access the underlying record, an additional request is required. Instead, you can specify the fields you want to retrieve the records for under the "expand" parameter. For example, if you list the records for a table:

```js
GET /api/data/departments?pageSize=1

{
  "records": [
    {
      "id": "rec1IuCvhVrg5YlLv",
      "fields": {
        "Department": "Accounting",
        "Description": "Accounting Department",
        "Employee": [
          "recHVuMuvuoJ9AQ3t"
        ],
        "Lark ID": "g6561df6b47f1452",
        "Last Modified": "2021-08-03T18:06:48.000Z"
      },
      "createdTime": "2021-02-15T08:36:09.000Z"
    }
  ]
}
```

You might want to expand the "Employee" field in those records. You can do so like this:

```js
GET /api/data/departments?pageSize=1&expand[]=Employee

{
  "records": [
    {
      "id": "rec1IuCvhVrg5YlLv",
      "fields": {
        "Department": "Accounting",
        "Description": "Accounting Department",
        "Employee": [
          "recHVuMuvuoJ9AQ3t"
        ],
        "Lark ID": "g6561df6b47f1452",
        "Last Modified": "2021-08-03T18:06:48.000Z"
      },
      "createdTime": "2021-02-15T08:36:09.000Z",
      "children": {
        "Employee": [
            {
              "id": "recHVuMuvuoJ9AQ3t",
              "fields": {
                "Last Name": "Kim",
                "First Name": "Seokjin",
                "Middle Name": "Bangtan",
                "Workplace": [
                  "rece1Ho3R05DPGart"
                ]
              },
              "createdTime": "2021-02-15T08:45:25.000Z"
            }
        ]
      }
    }
  ],
  "offset": "itrNrfACt08f1J8Yu/rec1IuCvhVrg5YlLv"
}
```

You can even expand child fields, but keep in mind that every expansion adds additional requests to the AirTable API.

```js
GET /api/data/departments?pageSize=1&expand[]=Employee&expand[]=Employee.Workplace

{
  "records": [
    {
      "id": "rec1IuCvhVrg5YlLv",
      "fields": {
        "Department": "Accounting",
        "Description": "Accounting Department",
        "Employee": [
          "recHVuMuvuoJ9AQ3t"
        ],
        "Lark ID": "g6561df6b47f1452",
        "Last Modified": "2021-08-03T18:06:48.000Z"
      },
      "createdTime": "2021-02-15T08:36:09.000Z",
      "children": {
        "Employee": [
          {
            "id": "recHVuMuvuoJ9AQ3t",
            "fields": {
              "Last Name": "Kim",
              "First Name": "Seokjin",
              "Middle Name": "Bangtan",
              "Workplace": [
                "rece1Ho3R05DPGart"
              ]
            },
            "createdTime": "2021-02-15T08:45:25.000Z",
            "children": {
              "Workplace": [
                {
                  "id": "rece1Ho3R05DPGart",
                  "fields": {
                    "Code": "RXS",
                    "Workplace": "ROXAS CITY",
                    "PDY ID": 2,
                    "Company": [
                      "rec5QOhfo69dQMPGx"
                    ]
                  },
                  "createdTime": "2021-04-29T10:29:11.000Z"
                }
              ]
            }
          }
        ]
      }
    }
  ],
  "offset": "itrhVYeJhCjW0pCpY/rec1IuCvhVrg5YlLv"
}
```

#### `ids`

To retrieve multiple records at once by ID, you can use the `ids` parameter in the list records endpoint.

```js
GET /api/data/department?ids[]=rec1IuCvhVrg5YlLv&ids[]=rec5EifDfuESbMIrF&ids[]=recBHGT7zLprgrfGR

{
    "records": [
        {
            "id": "rec1IuCvhVrg5YlLv",
            "fields": { ... },
            "createdTime": "2021-02-15T08:36:09.000Z"
        },
        {
            "id": "rec5EifDfuESbMIrF",
            "fields": { ... },
            "createdTime": "2020-11-16T17:05:38.000Z"
        },
        {
            "id": "recBHGT7zLprgrfGR",
            "fields": { ... },
            "createdTime": "2020-11-16T17:05:38.000Z"
        }
    ]
}
```

#### REST-ful CRUD

For convenience, the CRUD API can also be accessed in a REST-ful manner. In addition to being able to POST/PATCH an array to /api/data/[table-slug]/, you can also POST single objects to `/api/data/[table-slug]/` to create just a single record. You can also PATCH single objects to `/api/data/[table-slug]/[id]` to update a single record.

#### Multi-Update

The `children` key can passed be alongside create/update requests to create/update child records. For example:

```js
POST /api/data/test

{
  "fields": {
    "Name": "Test"
  },
  "children": {
    "Link 2": [
      {
        "fields": {
          "Name": "Test Linked"
        }
      }
    ]
  }
}
```

Returns:

```js
{
  "id": "recEEKiIPPDf6hKHy",
  "fields": {
    "Name": "Test",
    "Link 2": [
      "recEeXEFAYwWjqacu"
    ]
  },
  "createdTime": "2021-08-10T08:19:26.000Z",
  "children": {
    "Link 2": [
      {
        "id": "recEeXEFAYwWjqacu",
        "fields": {
          "Name": "Test Linked"
        },
        "createdTime": "2021-08-10T08:19:25.000Z"
      }
    ]
  }
}
```

To update a child record, just specify an ID:

```js
POST /api/data/test

{
  "fields": {
    "Name": "Test"
  },
  "children": {
    "Link 2": [
      {
        "id": "rec1LGFjykaPXr96x",
        "fields": {
          "Name": "Test"
        }
      }
    ]
  }
}
```

You can combine updates and creates under the same request.

```js
{
  "fields": {
    "Name": "Test"
  },
  "children": {
    "Link 2": [
      {
        "fields": {
          "Name": "Test Linked"
        }
      },
      {
        "id": "rec1LGFjykaPXr96x",
        "fields": {
          "Name": "Test"
        }
      }
    ]
  }
}
```

#### Delete By Formula

The `DELETE` request has been extended to allow you to delete by a formula, and also to delete more than 10 IDs at a time. Just pass the formula as `filterByFormula`.

```js
DELETE /api/data/test

{
  "filterByFormula": "{Name}=\"Test\""
}
```

You can even combine the `ids` and `filterByFormula` parameters to delete within a set of IDs.

```js
DELETE /api/data/test

{
  "ids": ["recm2PpK1caR1CWBD"],
  "filterByFormula": "{Name}=\"Test 1\""
}
```

## Validation

In addition to extending the API's capabilities, DENIM introduces a validation layer. This ensures that data is valid _before_ it's created/updated. Additionally, this validation is shared on both the client and server-side, so the client can validate user input even before it's submitted. Validation is defined on the model level, meaning that you define the validation for specific fields in a table, and this validation is reflected on both the API and in forms.

### Validation Extensions

By default, DENIM only inherits the validations defined in AirTable and converts them into a Yup schema. To extend these validations, you can edit the `./config/validation-extensions.js` file. For example, to make a field required, you can do this:

```js
import Yup from 'yup';

/**
 * @type {{
 *  [table: string]: {
 *    [column: string]: (validation: Yup.BaseSchema) => Yup.BaseSchema
 *  }
 * }}
 */
export default {
  'Field Type Test': {
    Text: (validation) => {
      return validation.required().nullable(false);
    },
  },
};
```

Which will set the "Text" field on the "Field Type Test" table to be required.

## UI

DENIM uses Ant Design for rapid creation of UI. To create a page, you can simply create a file under the `./pages/` folder and use the `AppLayout` component to set up your page. For example:

**index.js**

```js
import AppLayout from '../components/layout';

const Home = () => {
  return (
    <AppLayout
      title="Home"
      breadcrumb={[
        {
          key: 'app',
          label: 'DENIM v2',
          href: '/',
        },
        {
          key: 'home',
          label: 'Home',
        },
      ]}
      menuItemKey="home"
    >
      Some content goes here!
    </AppLayout>
  );
};

export default Home;
```

This will create a home page that already has a menu, breadcrumbs, and some text. To define what shows up in the menu, the theming, and anything else, simply edit the `./components/layout.js` file. `menuItemKey` corresponds to a `key` of a `Menu.Item` entry in the layout file. This simply tells the layout component which menu item is currently active. The `breadcrumb` entries require a "key" (for purposes of identification within React), a "label", and an optional "href" which is a link it can go to.

### Forms

DENIM leverages the Ant Design forms, and the Formik library to rapidly create and design forms. For more information on Formik, consult the [Formik documentation](https://formik.org/docs/overview) directly.

#### AirTable Helpers

Because DENIM statically caches the AirTable schema, we can leverage the schema to automatically generate forms based on fields. This can be done via the `AirTableForm` and `AirTableField` components. This form will also automatically inherit the validations defined on the table. Here's an example:

```js
import { ResetButton, SubmitButton } from 'formik-antd';
import AppLayout from '../components/layout';
import AirTableField from '../denim/components/AirTableField';
import AirTableForm from '../denim/components/AirTableForm';

const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

const Home = () => {
  return (
    <AppLayout
      title="Home"
      breadcrumb={[
        {
          key: 'app',
          label: 'DENIM v2',
          href: '/',
        },
        {
          key: 'home',
          label: 'Home',
        },
      ]}
      menuItemKey="home"
    >
      <AirTableForm
        table="Field Type Test"
        onSubmit={async (values) => {
          await sleep(500);
          alert(JSON.stringify(values, null, 2));
        }}
      >
        <AirTableField columnName="Text" />
        <SubmitButton>Submit</SubmitButton>
        <ResetButton>Reset</ResetButton>
      </AirTableForm>
    </AppLayout>
  );
};

export default Home;
```

This renders a form for the "Field Type Test" table, and renders the "Text" column as a text component. If you already know what component will be rendered for a given field, you can also pass any props that would normally be usable by that component, as per the [Ant Design documentation](https://ant.design/docs/react/introduce).

#### Link Fields

For "Link to another record" fields, a searchable dropdown is rendered. By default, this displays the leftmost column in the table, and searches based on that. If you want to render a different field, or change the formula used for searching, you can use the `lookupField` and `lookupFilterFormula` properties respectively. For example:

```js
<AirTableField
  columnName="Single Link"
  lookupField="Label"
  lookupFilterFormula="FIND($search, {Label}) > 0"
/>
```

This will change the dropdown to render the "Label" field instead, and search on it. `$search` is a special variable in the formula that represents the text that was typed into the search field. Additionally, you can provide the `lookupPreload` boolean property to load a few options first before any search has been inputted.

#### Form Submission

The `AirTableForm` component is a simple layer over the `Formik` component. To actually save data back to AirTable, you'll need to use the built-in `AirTableClient` functions. Here's a full example of a form that allows submission:

```js
import { Col, Row } from 'antd';
import { ResetButton, SubmitButton } from 'formik-antd';
import AppLayout from '../components/layout';
import AirTableClient from '../denim/airtable/airtable-client';
import AirTableField from '../denim/components/AirTableField';
import AirTableForm from '../denim/components/AirTableForm';

const Home = () => {
  return (
    <AppLayout
      title="Home"
      breadcrumb={[
        {
          key: 'app',
          label: 'DENIM v2',
          href: '/',
        },
        {
          key: 'home',
          label: 'Home',
        },
      ]}
      menuItemKey="home"
    >
      <AirTableForm
        table="Field Type Test"
        onSubmit={async (values) => {
          const createdRecord = await AirTableClient.create('Field Type Test', {
            fields: values,
          });

          console.log(createdRecord);
        }}
      >
        <Row gutter={24}>
          <Col span={12}>
            <AirTableField columnName="Text" />
          </Col>
          <Col span={12}>
            <AirTableField columnName="Long Text" />
          </Col>
        </Row>
        <SubmitButton>Submit</SubmitButton>
        <ResetButton>Reset</ResetButton>
      </AirTableForm>
    </AppLayout>
  );
};

export default Home;
```

#### Data Pre-Retrieval

When allowing updates for a record, you'll generally want the form to already have the record loaded. This way there's no delay between when the page loads, and when the user can start modifying the record. To do this, you can leverage Next.js's `getServerSideProps`. Note that when writing code for this function, you'll have to use the server-side functions, not `AirTableClient`. This will allow you to retrieve data, and pass it as properties to the component. Once that's retrieved and passed to the component, you can pass the values to the "existingRecord" prop for it to load everything. `diffValues` returns only the values that have changed in the record. Again, here's a full example, given this is a page named `[id].js`:

```js
import { Col, Row } from 'antd';
import { ResetButton, SubmitButton } from 'formik-antd';
import { useState } from 'react';
import AppLayout from '../components/layout';
import AirTableClient from '../denim/airtable/airtable-client';
import { retrieveRecord } from '../denim/airtable/data';
import AirTableField from '../denim/components/AirTableField';
import AirTableForm from '../denim/components/AirTableForm';

export async function getServerSideProps(context) {
  const { id } = context.query;

  // Retrieve the record, with expansion on the link fields.
  const record = await retrieveRecord('Field Type Test', id, {
    expand: ['Single Link', 'Multi Link'],
  });

  return {
    props: {
      record,
    },
  };
}

const Record = ({ record }) => {
  const [currentRecord, setCurrentRecord] = useState(record);

  return (
    <AppLayout
      title="Home"
      breadcrumb={[
        {
          key: 'app',
          label: 'DENIM v2',
          href: '/',
        },
        {
          key: 'home',
          label: 'Home',
        },
        {
          key: 'record',
          label: currentRecord.fields.Text,
          href: `/${record.id}`,
        },
      ]}
      menuItemKey="home"
    >
      <AirTableForm
        table="Field Type Test"
        onSubmit={async (values) => {
          const updatedRecord = await AirTableClient.update('Field Type Test', {
            id: record.id,
            fields: AirTableClient.diffValues(currentRecord, values),
          });

          setCurrentRecord(updatedRecord);

          return updatedRecord.fields;
        }}
        existingRecord={record}
      >
        <Row gutter={24}>
          <Col span={12}>
            <AirTableField columnName="Text" />
          </Col>
          <Col span={12}>
            <AirTableField columnName="Long Text" />
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={12}>
            <AirTableField
              columnName="Single Link"
              lookupField="Label"
              lookupFilterFormula="FIND($search, {Label}) > 0"
            />
          </Col>
          <Col span={12}>
            <AirTableField columnName="Multi Link" />
          </Col>
        </Row>
        <SubmitButton>Submit</SubmitButton>
        <ResetButton>Reset</ResetButton>
      </AirTableForm>
    </AppLayout>
  );
};

export default Record;
```

You can then connect the "create" form to redirect to the "update" form using the [Next.js Router](https://nextjs.org/docs/api-reference/next/router).

#### AirTableFormHelpers

For convenience, you can use the `AirTableFormHelpers` functions to generate submit handlers automatically.

### Views/Reporting

DENIM also provides various ways to display collections of data.

#### Tables

Since Ant Design provides many different ways to display tables, I've opted to just create a generic hook called `useAirTableCollection` you can use to retrieve the data, and leave the actual UI up to the developer. Nevertheless, here's a basic example. DENIM automatically generates the column renderers.

```js
import { Button, Table } from 'antd';
import { useMemo } from 'react';
import AppLayout from '../components/layout';
import { useAirTableCollection } from '../denim/components/useAirTableCollection';

const Home = () => {
  const { records, isRetrieving, retrieveMore, hasMore, columns } =
    useAirTableCollection(
      'Field Type Test',
      useMemo(
        () => [
          'Text',
          'Long Text',
          'Single Select',
          'Multi Select',
          'Date',
          'Date & Time',
          'Phone Number',
          'Email',
          'URL',
          'Checkbox',
          'Number',
          'Currency',
          'Percentage',
          'Duration',
          'Single Link',
          'Multi Link',
          'Rating',
          'Calculation',
        ],
        []
      )
    );

  return (
    <AppLayout
      title="Home"
      breadcrumb={[
        {
          key: 'records',
          label: 'Records',
          href: '/',
        },
      ]}
      menuItemKey="home"
    >
      <Table
        columns={columns}
        dataSource={records}
        pagination={false}
        loading={isRetrieving}
      />
      {hasMore ? (
        <div style={{ marginTop: 16, textAlign: 'right' }}>
          <Button onClick={retrieveMore} disabled={isRetrieving}>
            Load More
          </Button>
        </div>
      ) : null}
    </AppLayout>
  );
};

export default Home;
```

Other than the table and the columns, you can pass other options (e.x. a filterByFormula) to the hook. Adding an expansion here is also how you can pre-load link fields. Just rememeber to memoize this as well.

```js
const { records, isRetrieving, retrieveMore, hasMore, columns } =
  useAirTableCollection(
    'Field Type Test',
    useMemo(
      () => [
        'Text',
        'Long Text',
        'Single Select',
        'Multi Select',
        'Date',
        'Date & Time',
        'Phone Number',
        'Email',
        'URL',
        'Checkbox',
        'Number',
        'Currency',
        'Percentage',
        'Duration',
        'Single Link',
        'Multi Link',
        'Rating',
        'Calculation',
      ],
      []
    )
  );
```

Consult the [Ant Design Documentation](https://ant.design/components/table/) for more information on tables.

##### Prefetching

You can pass a record set and an offset to the "prefetch" property to preload a set of data. If you combine this with `getServerSideProps` you can prefetch the first set on the server side so the user can see it as soon as the page loads.

```js
import { useMemo } from 'react';
import { listRecords } from '../denim/airtable/data';
import { useAirTableCollection } from '../denim/components/useAirTableCollection';

export async function getServerSideProps() {
  // Retrieve the record, with expansion on the link fields.
  const { offset, records } = await listRecords('Field Type Test', {
    expand: ['Single Link', 'Multi Link'],
  });

  return {
    props: {
      prefetch: {
        offset: offset || null,
        records,
      },
    },
  };
}

const Home = ({ prefetch }) => {
  const {} = useAirTableCollection(
    'Field Type Test',
    useMemo(
      () => [
        'Text',
        'Single Select',
        'Multi Select',
        'Date',
        'Date & Time',
        'Phone Number',
        'Email',
        'URL',
        'Checkbox',
        'Number',
        'Currency',
        'Percentage',
        'Duration',
        'Single Link',
        'Multi Link',
        'Rating',
        'Calculation',
      ],
      []
    ),
    useMemo(
      () => ({
        expand: ['Single Link', 'Multi Link'],
      }),
      []
    ),
    prefetch
  );
};
```

## Authentication and Authorization

DENIM uses Lark for user authentication. To use this feature, provide a Lark app ID and Lark app secret (currently, ISV apps are not supported, only custom apps. To do so, either provide a LARK_APP_ID and LARK_APP_SECRET environment variable, or edit `config/lark.js` to provide the values. Then you can go to `/api/auth/login` to log in (or redirect unauthenticated users here). The authentication is pretty simple, so it's pretty simple to replace it with another authentication scheme. You'll need to pass the user to the props, and wrap `getServerSidewProps` with `withSession` and `withAuth` to enable authentication for that page.

```js
export const getServerSideProps = withSession(
  withAuth(async (ctx) => {
    return {
      props: {
        user: ctx.user,
      },
    };
  })
);
```

You can also use the `useUser` hook to retrieve the currently logged in user from anywhere in thee UI. (You'll have to pass the user to the props for this to work right)

### Dummy Authentication

Sometimes you may find it useful to use "dummy" authentication, which is not secure but allows you to test a multi-user scenario without needing to log into multiple Lark accounts. To accomplish this, provide a "DUMMY_AUTH_TABLE" environment variable, whose value is the name of an AirTable containing a list of users. Then, you'll need to edit `./config/dummy-auth.js` to configure the object which will be returned when one of these records is selected as a user.

### AirTable Integration

For all of the API proxy routes, "user" is passed to the `options` property. This means you can use the executing user to further filter or transform data by editing `./denim/airtable/data.js` directly. However, I would recommend a "hooks" pattern. So, whenever you modify `data.js`, instead of directly adding the functionality you need, you can set up a function like this in `./config/hooks/`:

```js
export const listRecordsHook = async (options) => {
  options.filterByFormula = 'AND(1, 1, 0)';
};
```

And call the function instead. This will minimize merge conflicts with the upstream `main` branch.

Also, remember that if you make requests directly to AirTable (such as when pre-fetching data in `getServerSideProps`), you need to pass the executing user to the options manually. For example:

```js
export const getServerSideProps = withSession(
  withAuth(async (ctx) => {
    const data = await listRecords('Field Type Test', {
      user: ctx.user,
    });

    return {
      props: {
        data,
      },
    };
  })
);
```

### Secure Endpoints

Another possible approach to authorization is to forgoe the data API entirely, and instead use the concept of secure endpoints to lock down what the user can access. This involves creating your own layer on top of the AirTable calls, and applying this to the generated AirTable grids, forms, and fields. DENIM has several useful utility functions for creating these.

#### updateRecords

Here's an example `secureUpdateRecordsHandler`, which is for the first registration page of CapizTalents:

```js
import { listRecords } from '../../../../denim/airtable/data';
import { quoteValue } from '../../../../denim/airtable/schema';
import { secureUpdateRecordsHandler } from '../../../../denim/secure-endpoints/update-records';
import { throwValidationError } from '../../../../denim/secure-endpoints/validation-error';
import withSession from '../../../../denim/session';

const handler = withSession(
  secureUpdateRecordsHandler({
    preUpdate: async (table, input) => {
      if (Array.isArray(input)) {
        throw new Error('Unsupported.');
      }

      Object.keys(input.fields).forEach((key) => {
        if (
          ![
            'Email',
            'Mobile No',
            'First Name',
            'Middle Name',
            'Last Name',
            'Registration Intent',
          ].includes(key)
        ) {
          delete input.fields[key];
        }
      });

      const { records: existing } = await listRecords('Users', {
        filterByFormula: `OR({Email}=${quoteValue(
          input.fields.Email
        )}, {Mobile No}=${quoteValue(input.fields['Mobile No'])})`,
        maxRecords: 1,
        fields: ['ID'],
      });

      if (existing.length) {
        throwValidationError('Email or mobile number already exists');
      }

      return ['Users', input, true];
    },
    postUpdate: async (record, req) => {
      // Only one record is expected.
      if (!process.env.DUMMY_AUTH_TABLE) {
        // ... Code to create Lark ID and save it to session
      }

      if (process.env.DUMMY_AUTH_TABLE) {
        req.session.set('lark_user_id', record.id);
        await req.session.save();
      }

      res.send({
        status: 'OK',
      });
    },
  })
);

export default handler;
```

In the `preUpdate`, we throw an error if multiple records are passed (only one user can register at a time). Additionally, it does a check to see if the email or mobile number already exists in AirTable, and throws a validation error if it does. Then, the return value locks down the table to "Users" and locks down `isCreate` to `true`. The `postUpdate` proceeds to authenticate the newly created user.

Now you can pass this endpoint to either the `submitCreateHandler` or `submitUpdateHandler` helpers if you're using them.

```js
          onSubmit={AirTableFormHelpers.submitCreateHandler('Users', {
            secureEndpoint: '/api/auth/register',
            router,
            redirectUrl: '/register/profile',
          })}
```

#### listRecords

Secure `listRecords` endpoints work in pretty much the same way.

```js
import { secureListRecordsHandler } from '../../../denim/secure-endpoints/list-records';
import { rolePage } from '../../../utils/role-page';

const validFields = ['School Name', 'School ID', 'School Type'];

const handler = rolePage(
  secureListRecordsHandler({
    preRetrieve: async (table, options) => {
      return [
        'Schools',
        {
          ...options,
          expand: [],
          fields:
            options.fields?.filter((f) => validFields.includes(f)) ||
            validFields,
          filterByFormula: options.filterByFormula
            ? 'OR(FIND(LOWER($search), LOWER({School ID}))>1, FIND(LOWER($search), LOWER({School Name}))>1, FIND(LOWER($search), LOWER({School Type}))>1)'.replace(
                /\$search/g,
                options.filterByFormula
              )
            : undefined,
        },
      ];
    },
  }),
  'aspirant'
);

export default handler;
```

This example locks down most options, and substitutes its own `filterByFormula`. Since this endpoint is passed to a lookup field via the `lookupEndpoint` property, we have some control over what is passed as the formula. Secure endpoints are passed to lookup fields via the `lookupEndpoint` property.

```js
<AirTableField
  columnName="School"
  lookupField={(record) => {
    return (
      <>
        {record.fields['School Name']}
        &nbsp;
        <Tag>{record.fields['School ID']}</Tag>
        <Tag>{record.fields['School Type']}</Tag>
      </>
    );
  }}
  lookupFilterFormula="$search"
  lookupPreload
  lookupEndpoint="/api/aspirant/schools"
/>
```

#### Throwing Errors

You can throw errors that are actually handled downstream in the UI by sending them as validation errors.

```js
const error = new Error();
error.isValidationError = true;
error.validationErrors = [{ message: 'Some error here' }];

throw error;
```

You can use the `throwValidationError` helper class (see earlier examples) if you're just throwing one message.

#### Disabling the Open Endpoints

The environment variable `DISABLE_OPEN_ENDPOINTS` will disable all the /api/data/ endpoints.

##### Global Secure Endpoints

You can also override the default endpoints by editing the `./config/secure-endpoints.js` file and configuring the endpoints there by table name. This will also disable the main endpoints for that table, so all requests will have to go through the secure endpoint instead.

## Job Queue

DENIM implements a system you can use to execute long-running background processes. This works by queuing jobs for processing and providing jobs a way to update data about them. DENIM doesn't enforce any specific progress system, so you're free to use it as needed. Jobs are defined in the `config/jobs.js` file, and must have a unique ID by which they are executed. Job functions are passed two arguments: `options` which is the data passed in the initial queue request, and `updateState` which is a callback that can be executed to update the current "state" of the job. This can be used to update progress while the job is running. To queue a job, you can use the JobQueue functions:

```js
let jobId = await JobQueue.enqueueJob('test', {});
```

This will start executing the function "test" in the `jobs.js` file in the background. It'll then return a job ID which you can use to query the job's state. To retrieve the state of any given job:

```js
const jobState = await JobQueue.getJobState(id);
```

Where "id" is the job ID of the queued job. If a job update its state with a `__final` key:

```js
updateState({
  state: 'COMPLETED',
  progress: 7,
  __final: true,
});
```

Then the next time its state is queried, it will be deleted. This should be done at the end of every job function, so that it frees up memory for other processes.

### Executing Code from Node.js

You may have noticed that when trying to execute code from raw node.js (e.x. with `node somefile.js`), you get an error such as `Cannot use import statement outside a module`. You can execute any arbitrary code by wrapping it in an API route, and calling this from `server.js` (or export the `handle` function and do it from another piece of code):

```js
const apiFromNode = require('./denim/job-queue/api-from-node');

apiFromNode(handle, 'data');
```

Note that these routes will be executed with the pseudo-HTTP method "INTERNAL" (as opposed to GET/POST/PUT/PATCH/DELETE) so you can easily identify them.

## Customizing the UI

Ant Design provides a lot of options for customization. DENIM compiles Ant Design's LESS at build-time (only once when you run `yarn dev` or `yarn build`). To modify LESS variables and otherwise customize Ant Design to your needs, you can modify `./components/antd.less`.

## Sample Project

Check out the `samples/demo` branch for some examples on how to set things up.
