import { Card, Select } from 'antd';
import { useRouter } from 'next/router';
import { listRecords } from '../../denim/airtable/data';
import { findTableSchema } from '../../denim/airtable/schema';
import withAuth from '../../denim/auth';
import withSession from '../../denim/session';

export const getServerSideProps = withSession(withAuth(async (ctx) => {
  if (ctx.user) {
    return {
      redirect: {
        destination: '/',
      }
    };
  }

  const { records: users } = await listRecords(process.env.DUMMY_AUTH_TABLE, {
    all: true,
  });

  return {
    props: {
      users,
      nameColumn: findTableSchema(process.env.DUMMY_AUTH_TABLE).columns[0],
    },
  };
}));

const DummyLogin = ({
  users,
  nameColumn,
}) => {
  const router = useRouter();

  return (
    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
      <div style={{ height: '200px' }} />
      <Card title="Dummy Login: Select a User" style={{ width: '75%' }}>
        <Select
          style={{ width: '100%' }}
          onChange={(value) => {
            router.push('/api/auth/dummy/' + value);
          }}
          showSearch
          placeholder="Select a user"
          optionFilterProp="children"
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {users.map((user) => (
            <Select.Option key={user.id} value={user.id}>
              {user.fields[nameColumn.name]}
            </Select.Option>
          ))}
        </Select>
        <div style={{ color: 'rgb(100, 100, 100)' }}>Note: This is authentication is only a placeholder and should not be used in a production environment.</div>
      </Card>
    </div>
  );
};

export default DummyLogin;
