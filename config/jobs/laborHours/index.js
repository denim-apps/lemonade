import PaydayClient from '../../../connections/payday';
import { listRecords } from '../../../denim/airtable/data';
import { quoteValue } from '../../../denim/airtable/schema';
import larkAdminApi from '../../../lark-admin/admin-api';
import { processDay } from './calculate-attendance';
import { retrieveAttendance, retrieveAttendanceUsers, retrieveHolidayTypesByDate } from './lark';
import { convertLarkAttendance } from './utils';

/**
 * @param {any} options 
 * @param {async (data: any) => void} updateState 
 */
const laborHours = async (options, updateState) => {
  updateState({
    options,
    step: 0,
    stepText: 'Matching employees...',
    totalSteps: 4,
  });

  // Ensure Lark admin.
  await larkAdminApi.init();

  // Retrieve employee IDs within this pay period.
  const payrollPeriod = await PaydayClient.getPayrollPeriod(options.payrollPeriod);
  const groupId = payrollPeriod.Grouping.Id;
  const { records: airtableEmployees } = await listRecords('Employee', {
    fields: ['User ID', 'Employee ID', 'Full Name', 'Email', 'Mobile Number', 'Pay Basis'],
    filterByFormula: `{Payroll Group ID}=${quoteValue(String(groupId))}`,
    all: true,
    ids: options.employees?.length ? options.employees : null,
    expand: ['Pay Basis'],
  });

  // Retrieve attendance data from Lark.
  const missingLarkUsers = [];
  const larkIds = [];

  for (let i = 0; i < airtableEmployees.length; i++) {
    let found = false;
    const byEmail = airtableEmployees[i].fields['Email'] ? await retrieveAttendanceUsers(
      airtableEmployees[i].fields.Email
    ) : [];

    if (byEmail.length) {
      larkIds.push(byEmail[0].LarkId);
      airtableEmployees[i].LarkUserId = byEmail[0].LarkId;
      found = true;
    }

    if (!found && airtableEmployees[i].fields['Mobile Number']) {
      const byPhone = await retrieveAttendanceUsers(
        airtableEmployees[i].fields['Mobile Number']
      );

      if (byPhone.length) {
        larkIds.push(byPhone[0].LarkId);
        airtableEmployees[i].LarkUserId = byPhone[0].LarkId;
        found = true;
      }
    }

    if (!found && airtableEmployees[i].fields['Full Name']) {
      const byFullName = await retrieveAttendanceUsers(
        airtableEmployees[i].fields['Full Name']
      );

      if (byFullName.length === 1) {
        larkIds.push(byFullName[0].LarkId);
        airtableEmployees[i].LarkUserId = byFullName[0].LarkId;
        found = true;
      }
    }

    if (!found) {
      missingLarkUsers.push(airtableEmployees[i].fields['User ID']);

      updateState({
        missingLarkUsers,
      });
    }
  }

  updateState({
    step: 1,
    stepText: 'Retrieving attendance...',
  });

  const attendanceData = (await retrieveAttendance(
    new Date(String(payrollPeriod.Start)),
    new Date(String(payrollPeriod.End)),
    larkIds,
  )).map(convertLarkAttendance);

  updateState({
    step: 2,
    stepText: 'Retrieving holidays...',
  });

  // Retrieve holidays from Lark.
  const holidayTypesByDate = await retrieveHolidayTypesByDate();

  updateState({
    step: 3,
    stepText: 'Calculating...',
  });

  // Perform calculations.
  const reportData = [];

  for (let i = 0; i < attendanceData.length; i++) {
    const day = attendanceData[i];
    const employee = airtableEmployees.find(({ LarkUserId }) => LarkUserId == day.UserId);

    if (employee) {
      const employeeId = employee.fields['Employee ID'];
      day['Employee ID'] = employeeId;
      day['Employee Name'] = employee.fields['Full Name'];

      const calculated = processDay(
        day,
        payrollPeriod.Id,
        holidayTypesByDate[String(attendanceData[i].Date)],
        employee.children['Pay Basis']?.[0]?.fields['Name'],
      );

      reportData.push(calculated);
    }
  }

  updateState({
    step: 4,
    stepText: 'Summarizing...',
    reportData,
  });

  // Summarize.
  const perEmployeeSummaries = {};

  reportData.forEach((row) => {
    if (!perEmployeeSummaries[String(row['Employee ID'])]) {
      perEmployeeSummaries[String(row['Employee ID'])] = {
        ...row,
      };

      delete perEmployeeSummaries[String(row['Employee ID'])].Date;
    } else {
      Object.keys(row).forEach((key) => {
        if (typeof row[key] === 'number' && key !== 'Payroll Period ID') {
          perEmployeeSummaries[String(row['Employee ID'])][key] += row[key];
        }
      });
    }
  });

  updateState({
    stepText: 'Complete.',
    __final: true,
    summary: Object.values(perEmployeeSummaries),
    step: 5,
  });
};

export default laborHours;
