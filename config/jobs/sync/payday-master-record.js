import got from 'got';
import PaydayClient from '../../../connections/payday';
import { listRecords, updateRecords } from '../../../denim/airtable/data';
import LarkApi from '../../../denim/lark/api';
import diffValues from '../../../denim/utils/diff-values';
import syncLog from './sync-log';
import createSyncer from './syncer';

const convertThroughMap = (record, map) => {
  const fields = {
    'PDY ID': record.Id,
  };

  Object.keys(map).forEach((key) => {
    fields[map[key]] = record[key];
  });

  return fields;
};

const paydayMasterRecordsSync = createSyncer({
  name: 'PayDay Master Record',
  init: async (options, context) => {
    console.log(`[PAYDAY MASTER RECORDS] Syncing ${options.paydayTable} => ${options.airtable}...`);
  },
  destinationRetriever: async (options, context) => {
    console.log(`[PAYDAY MASTER RECORDS] Retrieving ${options.airtable}...`);
    const { records } = await listRecords(options.airtable, {
      all: true,
    });

    return records;
  },
  sourceRetriever: async (options, context) => {
    console.log(`[PAYDAY MASTER RECORDS] Retrieving ${options.paydayTable}...`);
    return PaydayClient.retrieve(options.paydayTable);
  },
  sourceItemValid: async () => true,
  findInDestination: (airtableRecords, paydayRecord) => {
    return airtableRecords.find((record) => record.fields['PDY ID'] === paydayRecord.Id);
  },
  convertSourceToDestination: (paydayRecord, options) => {
    return convertThroughMap(paydayRecord, options.map);
  },
  requiresUpdate: (convertedSourceItem, destinationItem) => {
    const difference = diffValues(destinationItem.fields, convertedSourceItem);

    return Object.keys(difference).length;
  },
  destinationUpdator: async (convertedSourceItem, destinationItem, airtableEmployee, options, context) => {
    const difference = diffValues(destinationItem.fields, convertedSourceItem);

    await updateRecords(options.airtable, {
      id: destinationItem.id,
      fields: difference,
    });
  },
  destinationCreator: async (convertedSourceItem, airtableEmployee, options, context) => {
    await updateRecords(options.airtable, {
      fields: convertedSourceItem,
    }, true);
  },
});

export default paydayMasterRecordsSync;
