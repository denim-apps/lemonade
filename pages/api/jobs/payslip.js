import withAuth from '../../../denim/auth';
import JobQueue from '../../../denim/job-queue';
import withSession from '../../../denim/session';

const handler = withSession(
  withAuth(
    async (req, res) => {
      if (req.method === 'POST') {
        let jobId = await JobQueue.enqueueJob('payslip', {
          ...req.body,
        });
        await req.session.set('payslip_report_id', jobId);
        await req.session.save();

        return res.json({
          id: jobId,
        });
      }

      const { id } = req.query;

      return res.json(
        await JobQueue.getJobState(id, Boolean(req.query.longPoll))
      );
    }
  )
);

export default handler;
