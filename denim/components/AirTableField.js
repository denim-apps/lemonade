import { Checkbox, DatePicker, Form, Input, Rate, Select } from 'formik-antd';
import { Input as AntInput, Spin } from 'antd';
import numeral from 'numeral';
import React, { useEffect, useMemo, useState } from 'react';
import { getAirTableNumberFormat } from '../airtable/validation';
import { Field, useField } from 'formik';
import { findTableSchema, quoteValue } from '../airtable/schema';
import QueryString from 'qs';
import { useAirTableSchema } from './AirTableSchemaProvider';
import { useAirTableCache } from './AirTableCache';
import LemonadeLabel from '../../components/lemonade-label';
import AirTableClient from '../airtable/airtable-client';

/**
 * @type {React.FunctionComponent<{
 *  format: string
 * } & import('formik-antd').InputProps>}
 */
export const NumberFormattedInputField = ({
  format,
  onChange,
  value,
  onBlur,
  ...props
}) => {
  const [isFocused, setIsFocused] = useState(false);
  const [enteredInput, setEnteredInput] = useState('');

  const formatValue = (value) => {
    if (value !== undefined && value !== null) {
      // First attempt to parse the value.
      const parsed = numeral(value);
      const parsedValue = parsed.value();

      if (parsedValue !== null && !isNaN(parsedValue)) {
        return parsed.format(format);
      }
    }

    return value;
  };

  return (
    <AntInput
      onChange={(e) => {
        setEnteredInput(e.target.value);
      }}
      onFocus={(e) => {
        setEnteredInput(formatValue(value));
        setIsFocused(true);
      }}
      onBlur={(e) => {
        const parsed = numeral(e.target.value);
        const parsedValue = parsed.value();

        onChange((format && parsedValue !== null && !isNaN(parsedValue)) ? parsedValue : e.target.value);
        setIsFocused(false);
        onBlur && onBlur(e);
      }}
      value={isFocused ? enteredInput : formatValue(value)}
      {...props}
    />
  );
};

/**
 * @type {React.FunctionComponent<{
 *  columnName: string,
 *  lookupFilterFormula: string,
 *  lookupField: string,
 *  lookupPreload: boolean,
 *  lookupEndpoint: string,
 *  multiSelectAsCheckboxes: boolean,
 * }>}
 */
const AirTableField = ({
  columnName,
  lookupFilterFormula,
  lookupField,
  lookupPreload,
  lookupEndpoint,
  multiSelectAsCheckboxes,
  label = '',
  ...inputProps
}) => {
  const { tableSchema, shape } = useAirTableSchema();

  if (tableSchema) {
    const columnSchema = tableSchema.columns.find(({ name }) => name === columnName);
    const columnValidation = shape[columnName];
    const isRequired = columnValidation.spec.presence === 'required';

    if (columnSchema) {
      let component = null;

      if (columnSchema.type === 'text' || columnSchema.type === 'phone') {
        component = (
          <Input
            name={columnName}
            {...inputProps}
          />
        );
      } else if (columnSchema.type === 'multilineText') {
        component = (
          <Input.TextArea
            name={columnName}
            {...inputProps}
            rows={4}
          />
        );
      } else if (columnSchema.type === 'checkbox') {
        component = (
          <Checkbox
            name={columnName}
            {...inputProps}
          />
        );
      } else if (columnSchema.type === 'multiSelect' || columnSchema.type === 'select') {
        const choices = columnSchema.typeOptions?.choiceOrder.map((choice) => {
          return (
            <Select.Option key={columnSchema.typeOptions?.choices[choice].name}>
              {columnSchema.typeOptions?.choices[choice].name}
            </Select.Option>
          );
        });

        if (columnSchema.type === 'multiSelect') {
          if (multiSelectAsCheckboxes) {
            component = (
              <Checkbox.Group
                name={columnName}
                {...inputProps}
                options={columnSchema.typeOptions?.choiceOrder.map((choice) => {
                  return {
                    label: columnSchema.typeOptions?.choices[choice].name,
                    value: columnSchema.typeOptions?.choices[choice].name,
                  };
                })}
              />
            );
          } else {
            component = (
              <Select
                name={columnName}
                {...inputProps}
                mode="tags"
                placeholder="Select options"
              >
                {choices}
              </Select>
            );
          }
        } else if (columnSchema.type === 'select') {
          component = (
            <Select
              name={columnName}
              {...inputProps}
              placeholder="Select an option"
              allowClear
            >
              {choices}
            </Select>
          );
        }
      } else if (columnSchema.type === 'date') {
        component = (
          <DatePicker
            name={columnName}
            {...inputProps}
            showTime={columnSchema.typeOptions?.isDateTime}
          />
        );
      } else if (columnSchema.type === 'number') {
        const numberFormat = getAirTableNumberFormat(columnSchema);

        component = (
          <Field name={columnName}>
            {({
              field, // { name, value, onChange, onBlur }
              form: {
                setFieldValue,
              },
            }) => (
              <NumberFormattedInputField
                {...field}
                format={numberFormat}
                onChange={(value) => setFieldValue(field.name, value)}
              />
            )}
          </Field>
        );
      } else if (columnSchema.type === 'foreignKey') {
        const otherTableSchema = findTableSchema(columnSchema.typeOptions?.foreignTableId);

        if (otherTableSchema) {
          const displayField = lookupField || otherTableSchema.columns[0].name;

          component = (<AirTableLookupField {...{ columnName, otherTableSchema, relationship: columnSchema.typeOptions?.relationship, displayField, lookupFilterFormula, lookupPreload, lookupEndpoint, ...inputProps }} />);
        }
      } else if (columnSchema.type === 'rating') {
        const max = columnSchema.typeOptions?.max;

        component = (
          <Rate
            name={columnName}
            {...inputProps}
            allowClear
            count={max}
          />
        );
      } else {
        console.log(columnSchema.type);
      }

      if (component) {
        return (
          <Form.Item
            name={columnName}
            required={isRequired}
            showValidateSuccess
          >
            {component}
            <LemonadeLabel required={isRequired}>{label || columnName}</LemonadeLabel>
          </Form.Item>
        );
      }
    }
  }

  return null;
};

export const AirTableLookupField = ({ columnName, otherTableSchema, relationship, displayField, lookupFilterFormula, lookupPreload, lookupEndpoint, ...inputProps }) => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const tableEndpoint = lookupEndpoint || AirTableClient.getTableEndpoint(otherTableSchema.name);

  useEffect(() => {
    let cancelled = false;
    let timeout = setTimeout(async () => {
      if (searchQuery || lookupPreload) {
        setSearchResults(null);
        let query = '';

        if (searchQuery) {
          const searchFormula = lookupFilterFormula || 'SEARCH(LOWER($search), LOWER({' + otherTableSchema.columns[0].name + '}))>0';
          query = '?' + QueryString.stringify({
            filterByFormula: searchFormula.replace(/\$search/g, quoteValue(searchQuery)),
          });
        }

        const result = await fetch(tableEndpoint + query);
        const { records } = await result.json();

        if (!cancelled) {
          setSearchResults(records);
        }
      }
    }, 200);

    return () => {
      cancelled = true;
      clearTimeout(timeout);
    };
  }, [searchQuery, tableEndpoint]);

  const { existingRecord } = useAirTableSchema();
  const [recordCache, setRecordCache] = useState({});
  const [{ value }] = useField(columnName);
  const cache = useAirTableCache();
  const find = (id) => {
    return recordCache[id] || existingRecord?.children?.[columnName]?.find(({ id: otherId }) => otherId === id);
  };

  const arrayValue = useMemo(() => {
    if (!value) {
      return value;
    }

    return Array.isArray(value) ? value : [value];
  }, [value]);

  useEffect(() => {
    const missingIds = (arrayValue || []).filter((id) => !find(id));

    cache.retrieve(otherTableSchema.name, ...missingIds).then((others) => {
      const retrieved = {};

      others?.forEach((record) => {
        retrieved[record.id] = record;
      });

      setRecordCache((cache) => ({
        ...cache,
        ...retrieved,
      }));
    });
  }, [arrayValue, cache]);

  useEffect(() => {
    if (searchResults?.length) {
      searchResults.forEach((record) => {
        cache.store(record);
        setRecordCache((cache) => ({
          ...cache,
          [record.id]: record,
        }))
      });
    }
  }, [searchResults, cache]);

  useEffect(() => {
    if (existingRecord?.children?.[columnName]?.length) {
      existingRecord?.children?.[columnName]?.forEach((record) => {
        cache.store(record);
      });
    }
  }, [existingRecord?.children?.[columnName], cache]);

  return (
    <Select
      name={columnName}
      {...inputProps}
      showSearch
      defaultActiveFirstOption={false}
      showArrow={false}
      filterOption={false}
      onSearch={setSearchQuery}
      allowClear
      mode={relationship === 'one' ? undefined : 'multiple'}
    >
      {!searchResults ? (
        <Select.Option disabled>
          <Spin />
        </Select.Option>
      ) : null}
      {(searchResults || []).map((record) => (
        <Select.Option key={record.id}>{typeof (displayField) === 'function' ? displayField(record) : record?.fields?.[displayField]}</Select.Option>
      ))}
      {(arrayValue || []).filter((id) => !(searchResults || []).find(({ id: otherId }) => id === otherId)).map((id) => {
        const record = find(id);

        return (
          <Select.Option key={id}>
            {(typeof (displayField) === 'function' ? displayField(record) : record?.fields?.[displayField]) || <>{id} <Spin /></>}
          </Select.Option>
        );
      })}
    </Select>
  );
}


export default AirTableField;
