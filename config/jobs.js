import laborHours from './jobs/laborHours';
import sync from './jobs/sync';
import payslip from './jobs/payslip';

export default {
  'labor-hours': laborHours,
  'sync': sync,
  'payslip': payslip,
};
