import { deleteRecords, listRecords, updateRecords } from '../../../../denim/airtable/data';
import { findTableSchema } from '../../../../denim/airtable/schema';
import withAuth from '../../../../denim/auth';
import withSession from '../../../../denim/session';
import secureEndpoints from '../../../../config/secure-endpoints';

const handler = async (req, res) => {
  if (process.env.DISABLE_OPEN_ENDPOINTS) {
    return res.status(404).end();
  }

  try {
    const { table } = req.query;
    const tableSchema = findTableSchema(table);

    if (tableSchema) {
      if (req.method === 'GET') {
        if (secureEndpoints[tableSchema.name]?.list || process.env.DISABLE_OPEN_ENDPOINTS) {
          return res.status(404).end();
        }
  
        return res.json(await listRecords(table, {
          ...req.query,
          user: req.user,
        }));
      }

      if (req.method === 'POST' || req.method === 'PATCH') {
        if (secureEndpoints[tableSchema.name]?.update || process.env.DISABLE_OPEN_ENDPOINTS) {
          return res.status(404).end();
        }
  
        return res.json(await updateRecords(table, req.body, req.method === 'POST', req.user));
      }

      if (req.method === 'DELETE') {
        if (secureEndpoints[tableSchema.name]?.destroy || process.env.DISABLE_OPEN_ENDPOINTS) {
          return res.status(404).end();
        }
  
        return res.json(await deleteRecords(table, {
          ...req.body,
          user: req.user,
        }));
      }
    }

    res.status(404).end();
  } catch (e) {
    if (e.isValidationError) {
      res.status(422).send({
        errors: e.validationErrors,
      });
      return;
    }

    if (e.response) {
      res.setHeader('Content-Type', 'application/json');
      res.status(e.response.statusCode).send(e.response.body);
    } else {
      throw e;
    }
  }
};

export default withSession(withAuth(handler));
