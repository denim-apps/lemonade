/**
 * In the future, this might be on a more decentralized system. For now, everything just runs in one Node.js process.
 */
import jobs from '../../config/jobs';
import randomString from 'random-string';

const jobQueue = {};
const jobResolvers = {};

const JobQueue = {
  /**
   * Enqueues a job and passes the options as a parameter. Retuns the job ID.
   * 
   * @param {string} id 
   * @param {any} options 
   * @returns {Promise<string>}
   */
  enqueueJob: async (id, options) => {
    const job = jobs[id];

    if (job) {
      const jobId = randomString();
      jobQueue[jobId] = {};
      jobResolvers[jobId] = [];

      console.log(`${id} started with id ${jobId} and options: ${JSON.stringify(options)}`);

      job(options, (newValues) => {
        jobQueue[jobId] = {
          ...jobQueue[jobId],
          ...newValues,
        };

        jobResolvers[jobId].forEach((resolve) => resolve());
        jobResolvers[jobId].splice(0, jobResolvers[jobId].length);
      }).then(() => {
        if (jobQueue[jobId] && !jobQueue[jobId].__final) {
          jobQueue[jobId] = {
            ...jobQueue[jobId],
            __final: true,
          };
        }
      }).catch((e) => {
        jobQueue[jobId] = {
          ...jobQueue[jobId],
          __final: true,
          error: e,
          errorMessage: e.response?.body || e.message,
        };

        console.error('Job ' + id + ' failed.', e);

        if (e.response) {
          console.error(e.response.body);
        }
      });

      return jobId;
    }

    throw new Error(`Unknown job ${id}`);
  },
  /**
   * Retrieves the current state for a job.
   * 
   * @param {string} jobId 
   * @param {boolean} longPoll
   * @returns {Promise<any>}
   */
  getJobState: async (jobId, longPoll) => {
    if (!jobQueue[jobId]) {
      throw new Error(`Unknown job ID ${jobId}`);
    }

    if (longPoll && !jobQueue[jobId].__final) {
      await new Promise((resolve) => {
        let resolved = false;

        jobResolvers[jobId].push(() => {
          resolved = true;
          resolve();
        });

        setTimeout(() => {
          if (!resolved) {
            resolve();
          }
        }, 5000);
      });
    }

    const stateData = {
      ...jobQueue[jobId],
    };

    if (stateData.__final) {
      delete jobQueue[jobId];
    }

    return stateData;
  },
  /**
   * Checks if a job exists.
   * 
   * @param {string} jobId 
   * @returns {Promise<boolean>}
   */
  jobExists: async (jobId) => {
    return jobId in jobQueue;
  },
};

export default JobQueue;
