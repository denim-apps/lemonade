import got from 'got';
import { postUserRetrieval } from '../config/hooks';
import DummyAuth from '../config/dummy-auth';
import { createCacher } from '../utils/cacher';
import { retrieveRecord } from './airtable/data';
import LarkApi from './lark/api';

const retrieveUser = createCacher('user', {
  retrieve: async (userId) => {
    if (process.env.DUMMY_AUTH_TABLE) {
      const record = await retrieveRecord(process.env.DUMMY_AUTH_TABLE, userId);
      return await DummyAuth(record);
    } else {
      const accessToken = await LarkApi.getAppAccessToken();
      const {
        data: {
          user,
        },
      } = await got('https://open.larksuite.com/open-apis/contact/v3/users/' + userId + '?user_id_type=open_id', {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }).json();

      return user;
    }
  },
});

/**
 * @param {*} handler 
 * @returns {(...args: any[]) => Promise<any>}
 */
const withAuth = (handler) => async (req, res) => {
  const userId = (req.req || req).session.get('lark_user_id');
  const authExpiry = (req.req || req).session.get('auth_expiry');

  if (userId && (!authExpiry || Date.now() < authExpiry)) {
    const user = await retrieveUser(userId);

    // Set auth expiry to 24 hours.
    (req.req || req).session.set('auth_expiry', Date.now() + (24 * 60 * 60 * 1000));
    await (req.req || req).session.save();
    req.user = user;
  }

  await postUserRetrieval(req);
  return handler(req, res);
};

/**
 * @param {*} handler 
 * @returns {(...args: any[]) => Promise<any>}
 */
export const requireAuth = (handler) => async (req, res) => {
  if (!req.user) {
    if (res) {
      return res.redirect('/api/auth/login');
    }

    return {
      redirect: {
        destination: '/api/auth/login',
      },
    };
  }

  return handler(req, res);
};

export default withAuth;
