import { listRecords, updateRecords } from '../../../denim/airtable/data';
import { quoteValue } from '../../../denim/airtable/schema';

const syncLog = async (employeeRecordId, key, errorMessage) => {
  const { records: [logRecord] } = await listRecords('Sync Logs', {
    filterByFormula: `AND({Key}=${quoteValue(key)}, {Employee Record ID}=${quoteValue(employeeRecordId)})`,
    fields: ['Error'],
  });

  if (logRecord) {
    if (logRecord.fields['Error'] !== errorMessage) {
      await updateRecords('Sync Logs', {
        id: logRecord.id,
        fields: {
          Error: errorMessage,
        },
      });
    }
  } else {
    await updateRecords('Sync Logs', {
      fields: {
        Employee: [employeeRecordId],
        Key: key,
        Error: errorMessage,
      }
    }, true);
  }
};

export default syncLog;
