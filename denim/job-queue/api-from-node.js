const apiFromNode = async (
  handle,
  path,
  query = { },
  body = { },
) => {
  handle({
    method: 'INTERNAL',
    headers: { },
    url: `/api/${path}`,
    body,
  }, {
    setHeader: () => { },
    getHeader: () => '',
    _implicitHeader: () => { },
    end: () => { },
    once: () => { },
  }, {
    pathname: `/api/${path}`,
    query,
  });
};

module.exports = apiFromNode;
