export const throwValidationError = (message) => {
  const error = new Error();
  error.isValidationError = true;
  error.validationErrors = [
    {
      message,
    },
  ];
  throw error;
};
