import { listRecords, updateRecords } from '../airtable/data';
import { quoteValue } from '../airtable/schema';

const upsertRecord = async (tableName, key, values) => {
  const { records: [record] } = await listRecords(tableName, {
    maxRecords: 1,
    filterByFormula: `{${key}} = ${quoteValue(values[key])}`,
  });

  if (record) {
    await updateRecords(tableName, {
      id: record.id,
      fields: values,
    });
  } else {
    await updateRecords(tableName, {
      fields: values,
    }, true);
  }
};

export default upsertRecord;
