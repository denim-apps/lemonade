export const getServerSideProps = async (ctx) => {
  return {
    redirect: {
      destination: '/portal/timekeeping',
    },
  };
};

const TimekeepingPortalRedirect = () => null;

export default TimekeepingPortalRedirect;
