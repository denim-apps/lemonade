import React, { createContext, useContext, useMemo } from 'react';
import AirTableClient from '../airtable/airtable-client';

/**
 * @type {React.Context<{
 *  retrieve: (table: string, ...ids: string[]) => Promise<import('../airtable/data').AirTableRecord[]>,
 *  store: (record: import('../airtable/data').AirTableRecord) => void,
 * }>}
 */
const AirTableCacheContext = createContext({
  retrieve: async (table, id) => { },
  store: (store) => { },
});

export const useAirTableCache = () => useContext(AirTableCacheContext);

const createCache = () => {
  const idPromiseMap = { };
  const idCache = { };
  const batches = { };

  const store = async (record) => {
    idCache[record.id] = record;
  };

  const hydrate = async (table, ...ids) => {
    const records = [];
    const pendingIds = [];
    const unretrievedIds = [];

    ids.forEach((id) => {
      if (idCache[id]) {
        records.push(idCache[id]);
      } else {
        pendingIds.push(id);
      }
    });

    pendingIds.forEach((id) => {
      if (idPromiseMap[id]) {
        records.push(
          new Promise((resolve) => {
            idPromiseMap[id].push(resolve);
          }),
        );
      } else {
        unretrievedIds.push(id);
      }
    });

    if (unretrievedIds.length > 0) {
      for (let i = 0; i < unretrievedIds.length; i++) {
        const id = unretrievedIds[i];
        idPromiseMap[id] = [];

        records.push(
          new Promise((resolve) => {
            idPromiseMap[id].push(resolve);
          })
        );
      }

      const { records: retrievedRecords } = await AirTableClient.list(table, {
        ids,
        all: true,
      });

      retrievedRecords.forEach((record) => {
        idPromiseMap[record.id].forEach((resolve) => {
          resolve(record);
        });

        idCache[record.id] = record;
        store(record);
      });
    }

    return await Promise.all(records);
  };

  return {
    retrieve: async (table, ...ids) => {
      if (!batches[table]) {
        batches[table] = [];
      }
      
      for (let i = 0; i < ids.length; i++) {
        const id = ids[i];

        if (!batches[table].includes(id)) {
          batches[table].push(id);
        }
      }

      await new Promise((resolve) => setTimeout(resolve, 1));
      await hydrate(table, ...batches[table]);

      return await hydrate(table, ...ids);
    },
    store,
  };
};

/**
 * @type {React.FunctionComponent}
 */
const AirTableCacheProvider = ({
  children,
}) => {
  const cache = useMemo(() => createCache(), []);

  return (
    <AirTableCacheContext.Provider value={{ retrieve: cache.retrieve, store: cache.store }}>
      {children}
    </AirTableCacheContext.Provider>
  );
};

export default AirTableCacheProvider;
