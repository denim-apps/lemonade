import admin from '../components/admin';

export const getServerSideProps = admin(
  async (ctx) => {
    if (ctx.user?.employee) {
      if (ctx.user.employee.fields['Is HR Admin'] || ctx.user.employee.fields['Is HR User']) {
        return {
          redirect: {
            destination: '/employees',
          },
        };
      } else {
        return {
          redirect: {
            destination: '/employees/me',
          },
        };
      }
    }

    return {
      props: { },
    };
  },
);

const Empty = () => null;

export default Empty;
