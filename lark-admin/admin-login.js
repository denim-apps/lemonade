const tough = require('tough-cookie');
const { default: got } = require('got');
const fakeDeviceInfo = 'device_id=0;device_name=Chrome;device_os=Windows;device_model=Chrome;lark_version=;channel=Release;package_name=lark;tt_app_id=1660';

const adminLogin = async (
  cookieJar,
  onQrCode = (code) => { },
) => {
  cookieJar = cookieJar || new tough.CookieJar();

  let adminResponse = await got('https://www.larksuite.com/admin/index', {
    cookieJar,
  });

  if (adminResponse.url.indexOf('https://www.larksuite.com/accounts/page/login') === 0) {
    console.log('Redirected to login. Getting QR...');

    const qrResponse = await got.post('https://www.larksuite.com/accounts/qrlogin/init', {
      cookieJar,
      json: {
        biz_type: null,
        redirect_uri: 'https://www.larksuite.com/admin/index',
      },
      headers: {
        'x-app-id': 13,
        'x-api-version': '1.0.3',
        'x-device-info': fakeDeviceInfo,
        'x-locale': 'en',
        'x-terminal-type': 2,
      },
      responseType: 'json',
    });

    if (!qrResponse.body.data || !qrResponse.body.data.next_step || qrResponse.body.data.next_step !== 'qr_login_polling') {
      throw new Error(qrResponse.body.message);
    }

    onQrCode(JSON.stringify({
      qrlogin: {
        token: qrResponse.body.data.step_info.token,
      }
    }));

    let crossLoginUri = '';

    const flowToken = qrResponse.headers['x-flow-key'];

    while (true) {
      const qrScanResponse = await got.post('https://www.larksuite.com/accounts/qrlogin/polling', {
        cookieJar,
        json: {
          biz_type: null,
        },
        headers: {
          'x-app-id': 13,
          'x-api-version': '1.0.3',
          'x-device-info': fakeDeviceInfo,
          'x-locale': 'en',
          'x-terminal-type': 2,
          'x-flow-key': flowToken,
        },
        responseType: 'json',
      });

      if (qrScanResponse.body.data.next_step === 'enter_app') {
        console.log('QR code scanned!');
        crossLoginUri = qrScanResponse.body.data.step_info.cross_login_uri;
        break;
      } else if (qrScanResponse.body.data.next_step !== 'qr_login_polling') {
        throw new Error(JSON.stringify(qrScanResponse.body.data));
      }
    }

    await got.get(crossLoginUri, {
      cookieJar,
      responseType: 'json',
    });
  }

  adminResponse = await got('https://www.larksuite.com/admin/index', {
    cookieJar,
  });

  return adminResponse;
};

module.exports = adminLogin;
