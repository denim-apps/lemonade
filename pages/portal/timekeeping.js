import { Button, Card, Divider, Progress, Result, Spin, Table, Tag } from 'antd';
import { WarningOutlined } from '@ant-design/icons';
import Text from 'antd/lib/typography/Text';
import Title from 'antd/lib/typography/Title';
import { Formik, useField } from 'formik';
import { Form, Select, SubmitButton } from 'formik-antd';
import moment from 'moment';
import { useEffect, useMemo, useState } from 'react';
import * as Yup from 'yup';
import admin from '../../components/admin';
import AppLayout from '../../components/layout';
import PaydayClient from '../../connections/payday';
import { findTableSchema, quoteValue } from '../../denim/airtable/schema';
import { AirTableLookupField } from '../../denim/components/AirTableField';
import JobQueue from '../../denim/job-queue';
import LemonadeLabel from '../../components/lemonade-label';

const URL = typeof(window) !== 'undefined' ? (window.webkitURL || window.URL) : null;

const reportColumns = [
  'Employee ID',
  'Name',
  'Date',
  'Payroll Days',
  'SA1',
  'SA2',
  'SA3',
  'SA4',
  'SA5',
  'SA6',
  'Other1',
  'Other2',
  'Other3',
  'Other4',
  'Other5',
  'Other6',
  'Absences',
  'Leaves',
  'Holidays',
  'Part Time',
  'Late',
  'Undertime',
  'Reg+NP',
  'RegOT',
  'RegOT+NP',
  'RegOTEx',
  'RegOTEx+NP',
  'LegOT',
  'LegOT+NP',
  'LegOTEx',
  'LegOTEx+NP',
  'SpOT',
  'SpOT+NP',
  'SpOTEx',
  'SPOtEx+NP',
  'RstOT',
  'RstOT+NP',
  'RstOTEx',
  'RstOTEx+Np',
  'LegRstOT',
  'LegRstOT+NP',
  'LegRstOTEx',
  'LegRstOTEx+NP',
  'SpRstOT',
  'SpRstOT+NP',
  'SpRstOTEx',
  'SpRstOTEx+NP',
  'Project',
  'Project Code',
  'Cost Center',
];

export const getServerSideProps = admin(async (ctx) => {
  let existingJob = ctx.req.session.get('labor_hours_report_id') || null;

  if (existingJob && !(await JobQueue.jobExists(existingJob))) {
    existingJob = null;
  }

  return {
    props: {
      payrollPeriods: await PaydayClient.getPayrollPeriods(),
      currentJobId: existingJob,
    },
  };
});

const generateCsv = (columns, data) => {
  let lines = [];
  lines.push(columns.join(','));


  data.forEach((row) => {
    lines.push(columns.map((col) => `"${String((row[col] === null || row[col] === undefined) ? '' : row[col]).replace(/"/g, '""')}"`).join(','));
  });

  return lines.join('\n');
};

const TimekeepingPortal = ({
  payrollPeriods,
  currentJobId,
}) => {
  const [jobId, setJobId] = useState(currentJobId);
  const [jobData, setJobData] = useState(null);

  const updateJobStatus = async (id, longPoll) => {
    const response = await fetch('/api/jobs/labor-hours?id=' + id + '&longPoll=' + longPoll);
    const data = await response.json();
    setJobData(data);

    if (!data.__final) {
      updateJobStatus(id, '1');
    }
  };

  useEffect(() => {
    if (jobId) {
      updateJobStatus(jobId, '');
    }
  }, [jobId]);

  const exportUrls = useMemo(() => {
    if (URL && jobData?.reportData) {
      const dailyReport = generateCsv(reportColumns, jobData.reportData);
      const summaryReport = generateCsv(reportColumns.filter((c) => c !== 'Date'), jobData.summary);

      const contentType = 'text/csv';
      const dailyReportCsv = new Blob([dailyReport], { type: contentType });
      const summaryReportCsv = new Blob([summaryReport], { type: contentType });
      
      return {
        daily: URL.createObjectURL(dailyReportCsv),
        summary: URL.createObjectURL(summaryReportCsv),
      };
    }

    return { };
  }, [jobData?.reportData, jobData?.summary]);

  return (
    <AppLayout
      title="Timekeeping Portal"
      breadcrumb={[
        {
          label: 'Timekeeping Portal',
          href: '/portal/timekeeping',
          key: 'timekeeping-portal',
        },
      ]}
      menuItemKey="timekeeping"
    >
      <Title>Timekeeping Portal</Title>
      <Text>
        Generate the total labor hours report in this page for uploading
        to the LemonadePay Portal.
      </Text>
      <Divider />
      {jobId && !jobData ? (
        <>
          <Spin />
        </>
      ) : (
        <>
          <Formik
            initialValues={jobData?.options || {}}
            validationSchema={Yup.object().shape({
              payrollPeriod: Yup.string().required().label('Payroll Period'),
              employees: Yup.array(Yup.string()).nullable(true),
            })}
            onSubmit={async (values) => {
              const response = await fetch('/api/jobs/labor-hours', {
                method: 'POST',
                body: JSON.stringify(values),
                headers: {
                  'Content-Type': 'application/json',
                },
              });
              const { id } = await response.json();
              setJobId(id);
              setJobData(null);
            }}
          >
            <Form layout="vertical">
              <Form.Item name="payrollPeriod" required>
                <Select name="payrollPeriod" allowClear>
                  {payrollPeriods.map(({ Id, Code, Desc, Start, End }) => (
                    <Select.Option key={Id}>
                      {Desc} ({Code})
                      &nbsp;
                      <Tag>
                        {moment(String(Start)).format(
                          'MMM DD, Y',
                        )}
                        &nbsp;-&nbsp;
                        {moment(String(End)).format(
                          'MMM DD, Y',
                        )}
                      </Tag>
                    </Select.Option>
                  ))}
                </Select>
                <LemonadeLabel required>Payroll Period</LemonadeLabel>
              </Form.Item>
              <EmployeeFilterField
                periods={payrollPeriods}
              />
              <SubmitButton>
                {jobData ? 'Restart' : 'Start'}
              </SubmitButton>
            </Form>
          </Formik>
          {jobData ? (
            <>
              <Divider />
              {jobData.missingLarkUsers ? (
                <Card title={<><WarningOutlined /> The following employees were not matched in Lark</>} size="small" style={{ marginBottom: 16 }}>
                  {jobData.missingLarkUsers.map((user) => (
                    <Tag key={user} color="yellow">
                      {user}
                    </Tag>
                  ))}
                </Card>
              ) : null}
              <div style={{ textAlign: 'center' }}>{jobData.stepText}</div>
              <Progress percent={jobData.step / jobData.totalSteps * 100} showInfo={false} />
              {jobData.reportData ? (
                <>
                  <Divider>Daily Report</Divider>
                  <Table
                    columns={reportColumns.map((name) => ({
                      title: name,
                      key: name,
                      dataIndex: name,
                      render: (text, record) => (
                        <div style={{ wordWrap: 'break-word', wordBreak: 'break-word', minWidth: '85px' }}>
                          {typeof (text) === 'number' ? (Math.floor(text * 100) / 100) : text}
                        </div>
                      ),
                    }))}
                    dataSource={jobData.reportData}
                    pagination={false}
                    scroll={{
                      x: true,
                      y: 450,
                    }}
                  />
                  <Button
                    href={exportUrls.daily}
                    download="daily_attendance_export.csv"
                  >
                    Export CSV
                  </Button>
                </>
              ) : null}
              {jobData.summary ? (
                <>
                  <Divider>Summary Report</Divider>
                  <Table
                    columns={reportColumns.filter((c) => c !== 'Date').map((name) => ({
                      title: name,
                      key: name,
                      dataIndex: name,
                      render: (text, record) => (
                        <div style={{ wordWrap: 'break-word', wordBreak: 'break-word', minWidth: '85px' }}>
                          {typeof (text) === 'number' ? (Math.floor(text * 100) / 100) : text}
                        </div>
                      ),
                    }))}
                    dataSource={jobData.summary}
                    pagination={false}
                    scroll={{
                      x: true,
                      y: 450,
                    }}
                  />
                  <Button
                    href={exportUrls.summary}
                    download="attendance_export.csv"
                  >
                    Export CSV
                  </Button>
                </>
              ) : null}
              {jobData.errorMessage ? (
                <Result status="error" title="Error" subTitle={jobData.errorMessage} />
              ) : null}
              {/*JSON.stringify(jobData)*/}
            </>
          ) : null}
        </>
      )}
    </AppLayout>
  );
};

const EmployeeFilterField = ({
  periods,
}) => {
  const [{ value: periodId }] = useField('payrollPeriod');
  const period = periods.find(({ Id }) => Id === Number(periodId));

  return (
    <Form.Item name="employees">
      <AirTableLookupField
        columnName="employees"
        otherTableSchema={findTableSchema('Employee')}
        relationship="many"
        displayField="User ID"
        disabled={!period}
        lookupFilterFormula={`AND(SEARCH(LOWER($search), LOWER({User ID}))>0, {Payroll Group ID}=${quoteValue(String(period?.Grouping?.Id))})`}
      />
      <LemonadeLabel>Filter Employees</LemonadeLabel>
    </Form.Item>
  );
}

export default TimekeepingPortal;
