import React from 'react';

/**
 * @type {React.FunctionComponent<{
 *  required: boolean,
 * }>}
 */
const LemonadeLabel = ({
  required,
  children,
}) => {
  return (
    <div className={`lemonade-field-label ${required ? 'required': ''}`}>
      {children}
    </div>
  );
};

export default LemonadeLabel;
