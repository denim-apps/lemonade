import got from 'got';
import LarkApi from '../../../denim/lark/api';
import withSession from '../../../denim/session';
import { invalidate } from '../../../utils/cacher';

const handler = withSession(
  async (req, res) => {
    if (req.user) {
      // User is already authenticated. Why are we here?
      return res.redirect('/');
    }

    // Get access token.
    const appAccessToken = await LarkApi.getAppAccessToken();

    const redirectUrl = req.session.get('redirect_url');

    const {
      data: {
        open_id,
      },
    } = await got('https://open.larksuite.com/open-apis/authen/v1/access_token', {
      method: 'POST',
      json: {
        app_access_token: appAccessToken,
        grant_type: 'authorization_code',
        code: req.query.code,
      },
    }).json();

    req.session.set('lark_user_id', open_id);
    req.session.set('auth_expiry', 0);
    await req.session.save();
    await invalidate('user', open_id);

    if (redirectUrl) {
      req.session.unset('redirect_url');
      return res.redirect(redirectUrl);
    }

    return res.redirect('/');
  }
);

export default handler;
