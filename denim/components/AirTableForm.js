import React, { useEffect, useMemo } from 'react';
import { Formik, useFormikContext } from 'formik';
import { Form } from 'formik-antd';
import Yup from 'yup';
import { createRecordValidator } from '../validation/record-validation';
import AirTableSchemaProvider, { useAirTableSchema } from './AirTableSchemaProvider';

const ImmediateValidation = ({ values }) => {
  const form = useFormikContext();

  useEffect(() => {
    form.validateForm(values).then((errors) => {
      form.setErrors(errors);

      form.setTouched(
        Object.keys(errors).reduce((last, next) => ({
          ...last,
          [next]: true,
        }), { }),
        true,
      );
    });
  }, [values]);

  return null;
};

const InnerForm = ({
  table,
  children,
  layout = 'horizontal',
  innerFormProps,
  additionalValidation = { },
  ...props
}) => {
  const {
    shape: validationShape,
    existingRecord,
  } = useAirTableSchema();

  const validationSchema = useMemo(() => {
    if (validationShape) {
      const validator = createRecordValidator({
        ...validationShape,
        ...additionalValidation,
      });

      /*
      Uncomment for debugging validator result
      const originalValidator = validator.validate;
 
      validator.validate = async (...args) => {
        console.log(args);
 
        try {
          const result = await originalValidator.call(validator, ...args);
          console.log(result);
 
          return result;
        } catch (e) {
          console.log(e);
          throw e;
        }
      };
      */

      return validator;
    }

    return null;
  }, [validationShape, existingRecord]);

  const initialValues = useMemo(() => {
    return existingRecord?.fields || { };
  }, [existingRecord]);

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={initialValues}
      key={existingRecord?.id || table}
      {...props}
    >
      <Form layout={layout} {...(innerFormProps || { })}>
        <>
          <ImmediateValidation values={initialValues} />
          {children}
        </>
      </Form>
    </Formik>
  );
};

/**
 * @type {React.FunctionComponent<{
 *  table: string,
 *  existingRecord: import('../airtable/data').AirTableRecord,
 *  additionalValidation: {
 *    [table: string]: {
 *      [column: string]: (validation: Yup.BaseSchema) => Yup.BaseSchema
 *    }
 *  },
 * } & import('formik').FormikConfig>}
 */
const AirTableForm = (props) => {
  return (
    <AirTableSchemaProvider table={props.table} existingRecord={props.existingRecord}>
      <InnerForm {...props} />
    </AirTableSchemaProvider>
  );
};

export default AirTableForm;
