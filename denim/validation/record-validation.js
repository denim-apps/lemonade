import Yup, * as yup from 'yup';

/**
 * @typedef {{
 *  [key: string]: Yup.BooleanSchema | Yup.ArraySchema | Yup.DateSchema | Yup.StringSchema | Yup.NumberSchema,
 * }} RecordValidationShape
 * @global
 */

/**
 * @param {RecordValidationShape} shape 
 * @param {boolean} isPartial
 */
export const createRecordValidator = (shape, isPartial) => {
  return yup.lazy((value) => {
    const usedShape = isPartial ? Object.keys(value).reduce((current, key) => {
      return {
        ...current,
        [key]: shape[key],
      };
    }, { }) : shape;

    return yup.object().shape(usedShape);
  });
};

/**
 * @param {Yup.BaseSchema} validator 
 * @param {any} value 
 */
export const validateRecord = (validator, value) => {
  try {
    const record = validator.validateSync(value, {
      abortEarly: false,
    });

    return {
      valid: true,
      record,
      errors: [],
    };
  } catch (e) {
    return {
      valid: false,
      record: value,
      errors: e.inner.map(({ path, type, message, errors }) => {
        return { path, type, message: errors[0] || message };
      }),
    };
  }
};
