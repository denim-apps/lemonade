/**
 * @param {string} payrollPeriod
 * @param {'Special' | 'Legal' | null} [holidayType]
 * @param {string} employeePayBasis
 */
export const processDay = (day, payrollPeriod, holidayType, employeePayBasis) => {
  const hoursComputation = {
    'Employee ID': day['Employee ID'],
    'Name': day['Employee Name'],
    'Payroll Period ID': payrollPeriod,
    'Date': day.Date,
    'Payroll Days': 0.0,
    'SA1': 0.0,
    'SA2': 0.0,
    'SA3': 0.0,
    'SA4': 0.0,
    'SA5': 0.0,
    'SA6': 0.0,
    'Other1': 0.0,
    'Other2': 0.0,
    'Other3': 0.0,
    'Other4': 0.0,
    'Other5': 0.0,
    'Other6': 0.0,
    'Absences': 0.0,
    'Leaves': 0.0,
    'Holidays': 0.0,
    'Part Time': 0.0,
    'Late': 0.0,
    'Undertime': 0.0,
    'Reg+NP': 0.0,
    'RegOT': 0.0,
    'RegOT+NP': 0.0,
    'RegOTEx': 0.0,
    'RegOTEx+NP': 0.0,
    'LegOT': 0.0,
    'LegOT+NP': 0.0,
    'LegOTEx': 0.0,
    'LegOTEx+NP': 0.0,
    'SpOT': 0.0,
    'SpOT+NP': 0.0,
    'SpOTEx': 0.0,
    'SPOtEx+NP': 0.0,
    'RstOT': 0.0,
    'RstOT+NP': 0.0,
    'RstOTEx': 0.0,
    'RstOTEx+Np': 0.0,
    'LegRstOT': 0.0,
    'LegRstOT+NP': 0.0,
    'LegRstOTEx': 0.0,
    'LegRstOTEx+NP': 0.0,
    'SpRstOT': 0.0,
    'SpRstOT+NP': 0.0,
    'SpRstOTEx': 0.0,
    'SpRstOTEx+NP': 0.0,
    'Project': 0.0,
    'Project Code': 0.0,
    'Cost Center': 0.0,
  };

  // 10 PM in seconds from 12 AM
  const nightDiffStart = 22 * 60 * 60;

  // 6 AM the next day in seconds from 12 AM
  const nightDiffEnd = 30 * 60 * 60;

  const record = {
    ...day,
  };

  if (
    record['First In'] &&
    record['Last Out'] &&
    record['Last Out'] < record['First In']
  ) {
    // Last out should be pushed to next day.
    record['Last Out'] += 24 * 60 * 60;
  }

  if (
    record['Shift Time In'] &&
    record['Shift Time Out'] &&
    record['Shift Time Out'] < record['Shift Time In']
  ) {
    // Time out should be pushed to next day.
    record['Shift Time Out'] += 24 * 60 * 60;
  }

  // Calculate break time.
  let breakTime =
    record['Shift Time Out'] && record['Shift Time In']
      ? record['Shift Time Out'] -
      record['Shift Time In'] -
      record['Required Duration']
      : 0;

  // Check if it's a rest day.
  const isRestDay = !record['Shift Name'] || record['Shift Name'] === '-';

  if (isRestDay) {
    // Subtract 1 hour from the actual duration for break.
    if (record['Actual Duration'] >= 60 * 60 * 9) {
      record['Actual Duration'] -= 60 * 60;
      breakTime = 60 * 60;
    }
  }

  // Calculate Overtime
  let overtimeSeconds = record['Overtime Hours'] * 60 * 60;

  if (holidayType || isRestDay) {
    // Holiday pay must be pre-approved.
    record['Actual Duration'] = overtimeSeconds;
    overtimeSeconds = overtimeSeconds - Math.min(overtimeSeconds, 8 * 60 * 60);
  } else {
    // Retrieve Late Time In
    hoursComputation['Late'] = record['Late Time'] / 60;

    // Calculate Undertime
    if (record['Actual Duration'] > 0 && record['Required Duration'] > 0) {
      hoursComputation['Undertime'] = Math.max(
        (record['Required Duration'] - record['Actual Duration']) / 60 / 60 -
        hoursComputation['Late'],
        0
      );
    }

    // Anything else would be considered an absence.
    if (employeePayBasis !== 'Daily') {
      hoursComputation['Leaves'] = record['Leave Time'];

      hoursComputation['Absences'] = Math.max(
        0,
        (record['Required Duration'] - record['Actual Duration']) / 60 / 60 -
        hoursComputation['Late'] -
        hoursComputation['Undertime'] -
        hoursComputation['Leaves']
      );

      if (String(record['Leave Type']).indexOf('PL') === 0) {
      } else {
        hoursComputation['Absences'] += hoursComputation['Leaves'];
        hoursComputation['Leaves'] = 0;
      }
    }

    if (hoursComputation['Undertime'] <= 5 && hoursComputation['Leaves'] > 0) {
      hoursComputation['Undertime'] = 0;
    }

    if (hoursComputation['Late'] <= 5 && hoursComputation['Leaves'] > 0) {
      hoursComputation['Late'] = 0;
    }
  }

  // Calculate actual hours worked
  const hours = (record['Actual Duration'] - overtimeSeconds) / 60 / 60;

  // Calculate the amount of minutes they spent in night differential.
  let npMinutes = 0;
  let npOtMinutes = 0;

  if (
    record['First In'] &&
    record['Last Out'] &&
    record['Last Out'] >= nightDiffStart &&
    record['First In'] <= nightDiffEnd
  ) {
    npMinutes =
      Math.min(nightDiffEnd, record['Last Out']) -
      Math.max(record['First In'], nightDiffStart);

    // Calculate night differential OT time (last out - OT hours = start of OT)
    npOtMinutes =
      Math.min(nightDiffEnd, record['Last Out']) -
      Math.max(record['Last Out'] - overtimeSeconds, nightDiffStart);

    // Some negatives may occur, negatives should be normalized to 0.
    npMinutes = Math.max(0, Math.max(0, npMinutes) / 60 - breakTime / 60);
    npOtMinutes = Math.max(0, npOtMinutes) / 60;
  }

  let prefix = holidayType ? (holidayType === 'Special' ? 'Sp' : 'Leg') : '';

  if (isRestDay) {
    prefix += 'Rst';
  }

  const computedOT =
    Math.round(overtimeSeconds / 60 / 60) - Math.round(npOtMinutes / 60);
  const computedNightPay = Math.round(npMinutes / 60);
  const computedNightPayOT = Math.round(npOtMinutes / 60);

  if (prefix) {
    hoursComputation[prefix + 'OT'] = hours;
    hoursComputation[prefix + 'OTEx'] = computedOT;
    hoursComputation[prefix + 'OT+NP'] =
      Math.max(0, computedNightPay - computedNightPayOT);
    hoursComputation[prefix + 'OTEx+NP'] = computedNightPayOT;
  } else {
    hoursComputation['Payroll Days'] = hours >= 5 ? 1 : hours >= 4 ? 0.5 : 0;
    hoursComputation['RegOT'] = computedOT;
    hoursComputation['Reg+NP'] = computedNightPay;
    hoursComputation['RegOT+NP'] = computedNightPayOT;
  }

  return hoursComputation;
};
