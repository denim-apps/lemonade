import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Image from 'next/image';
import { Layout, Menu, Breadcrumb } from 'antd';
import AppConfig from '../config/app';
import { useUser } from '../denim/components/UserProvider';
import AirTableCacheProvider from '../denim/components/AirTableCache';
import LogoImage from './logo.jpg';

const { Header, Content } = Layout;

/**
 * @type {React.FunctionComponent<{
 *  menuItemKey: string,
 *  breadcrumb: { label: string, href: string, key: string }[],
 *  title: string,
 * }>}
 */
const AppLayout = ({
  breadcrumb,
  menuItemKey,
  children,
  title,
}) => {
  const { user } = useUser();

  return (
    <AirTableCacheProvider>
      <Layout className="layout">
        <Head>
          <title>{title ? `${title} - ` : ''}{AppConfig.name}</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <Header>
          <div className="app-logo" style={{ position: 'relative', top: '5px' }}>
            <Image src={LogoImage} alt="LemonadeHR" width="167" height="50" />
          </div>
          <Menu theme="light" mode="horizontal" selectedKeys={[menuItemKey]}>
            {(user?.employee?.fields?.['Is HR Admin'] || user?.employee?.fields?.['Is HR User']) ? (
              <>
                <Menu.Item key="employees">
                  <Link href="/">Employees</Link>
                </Menu.Item>
                <Menu.Item key="timekeeping">
                  <Link href="/portal/timekeeping">Timekeeping Portal</Link>
                </Menu.Item>
                {process.env.NEXT_PUBLIC_ENABLE_PAYSLIP_PORTAL ? (
                  <Menu.Item key="payslip">
                    <Link href="/portal/payslip">Payslip Portal</Link>
                  </Menu.Item>
                ) : null}
              </>
            ) : (
              <>
                <Menu.Item key="home">
                  <Link href="/">Home</Link>
                </Menu.Item>
              </>
            )}
          </Menu>
        </Header>
        <Layout>
          <Layout style={{ padding: '0 24px 24px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>
                {AppConfig.name}
              </Breadcrumb.Item>
              {breadcrumb.map(({ label, href, key }) => {
                return (
                  <Breadcrumb.Item key={key}>
                    {href ? (
                      <Link href={href}>
                        <a>{label}</a>
                      </Link>
                    ) : (
                      label
                    )}
                  </Breadcrumb.Item>
                );
              })}
            </Breadcrumb>
            <Content className="main-content">
              {children}
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </AirTableCacheProvider>
  );
};

export default AppLayout;
