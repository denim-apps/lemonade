import lark from '../../../config/lark';
import absoluteOrigin from '../../../denim/absolute-origin';
import withAuth from '../../../denim/auth';
import withSession from '../../../denim/session';

const handler = withSession(
  withAuth(
    async (req, res) => {
      if (req.user) {
        // User is already authenticated. Why are we here?
        return res.redirect('/');
      }

      // Dummy auth.
      if (process.env.DUMMY_AUTH_TABLE) {
        return res.redirect('/auth/dummy');
      }

      const origin = absoluteOrigin(req);

      return res.redirect('https://open.larksuite.com/open-apis/authen/v1/index?redirect_uri='
        + encodeURIComponent(`${origin}/api/auth/callback`)
        + '&app_id='
        + lark.appId);
    }
  )
);

export default handler;
