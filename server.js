const { createServer } = require('http');
const { URL } = require('url');
const next = require('next');
const qs = require('qs');
const larkAdminApi = require('./lark-admin/admin-api');
const apiFromNode = require('./denim/job-queue/api-from-node');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const port = process.env.PORT || 3000;

app.prepare().then(() => {
  createServer((req, res) => {
    const parsedUrl = new URL(req.url, 'http://0.0.0.0');
    const { pathname, searchParams } = parsedUrl;

    handle(req, res, {
      pathname,
      query: qs.parse(searchParams.toString()),
    });
  }).listen(port, (err) => {
    if (err) throw err;
    console.log('> Ready on http://localhost:' + port);
  });
});

larkAdminApi.init();

// Start the sync process.
if (process.env.ENABLE_SYNC) {
  apiFromNode(handle, 'start-sync');
}
