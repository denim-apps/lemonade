import { Tag } from 'antd';
import { useEffect, useMemo, useState } from 'react';
import { findTableSchema } from '../airtable/schema';
import { useAirTableCache } from './AirTableCache';

/**
 * @type {import("react").FunctionComponent<{
 *  column: import("../airtable/schema-retriever").AirTableForeignKeyColumn,
 *  value?: string | string[],
 *  recordChildren?: import("../airtable/data").AirTableRecord[],
 *  displayColumn: string,
 *  Component: any,
 * }>}
 */
const AirTableForeignKeyRenderer = ({
  column,
  value,
  recordChildren,
  displayColumn,
  Component,
}) => {
  const { retrieve } = useAirTableCache();
  const [storedChildren, setChildren] = useState([]);
  const children = storedChildren.length ? storedChildren : (recordChildren || []);
  const [pendingRetrieval, setPendingRetrieval] = useState([]);
  const ids = useMemo(() => (value ? (Array.isArray(value) ? value : [value]) : []), [value]);
  const otherTable = useMemo(() => findTableSchema(column.typeOptions.foreignTableId), [column?.typeOptions?.foreignTableId]);

  useEffect(() => {
    setChildren(recordChildren || []);
    setPendingRetrieval([]);
  }, [recordChildren]);

  useEffect(() => {
    const missingIds = ids.filter((id) => {
      return !children.find(({ id: otherId }) => otherId === id) && !pendingRetrieval.includes(id);
    });

    if (missingIds.length) {
      setPendingRetrieval((ids) => ids.concat(missingIds));

      (async () => {
        const records = await retrieve(otherTable.name, ...missingIds);

        setChildren((c) => c.concat(records));
        setPendingRetrieval((ids) => ids.filter((id) => !records.find(({ id: otherId }) => otherId === id)));
      })();
    }
  }, [children, otherTable, ids, pendingRetrieval]);

  return ids.map((id) => {
    if (Component) {
      return (
        <Component key={id}>
          {children.find(({ id: otherId }) => otherId === id)?.fields[displayColumn || otherTable.columns[0].name] || id}
        </Component>
      );
    }

    return (
      <Tag key={id}>
        {children.find(({ id: otherId }) => otherId === id)?.fields[displayColumn || otherTable.columns[0].name] || id}
      </Tag>
    );
  });
};

export default AirTableForeignKeyRenderer;
