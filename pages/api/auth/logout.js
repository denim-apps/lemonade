import withAuth from '../../../denim/auth';
import withSession from '../../../denim/session';

const handler = withSession(
  withAuth(
    async (req, res) => {
      req.session.unset('lark_user_id');
      req.session.unset('auth_expiry');
      await req.session.save();
      return res.redirect('/');
    }
  )
);

export default handler;
