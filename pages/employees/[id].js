import { Col, Divider, Row, Space, Tabs, notification, Layout } from 'antd';
import { ResetButton, SubmitButton } from 'formik-antd';
import copyToClipboard from 'copy-to-clipboard';
import AppLayout from '../../components/layout';
import AirTableField from '../../denim/components/AirTableField';
import AirTableForm from '../../denim/components/AirTableForm';
import AirTableFormHelpers from '../../denim/airtable/form-helpers';
import { useRouter } from 'next/dist/client/router';
import admin from '../../components/admin';
import { retrieveRecord } from '../../denim/airtable/data';
import { useState } from 'react';
import { findTableSchema } from '../../denim/airtable/schema';
import { useFormikContext } from 'formik';

export const getServerSideProps = admin(async (ctx) => {
  const { id } = ctx.query;

  if (id && id !== 'new') {
    const record = await retrieveRecord('Employee', id, {
      user: ctx.user,
      expand: findTableSchema('Employee')
        .columns
        .filter(({ type }) => type === 'foreignKey')
        .map(({ name }) => name),
    });

    return {
      props: {
        existingRecord: record,
      },
    };
  }

  return { props: {} };
});

const Errors = () => {
  const { errors } = useFormikContext();

  if (Object.keys(errors).length === 0) {
    return null;
  }

  return (
    <Row style={{ marginBottom: 16 }}>
      <Space direction="vertical" style={{ color: 'red' }}>
        {Object.values(errors || {}).map((error) => (
          <div key={error}>{error}</div>
        ))}
      </Space>
    </Row>
  );
};

const EmployeeForm = ({
  existingRecord,
}) => {
  const [currentRecord, setCurrentRecord] = useState(existingRecord);

  const router = useRouter();
  const url = process.env.NEXT_PUBLIC_EMPLOYEE_FORM_URL || 'https://airtable.com/shrMs1b9PvJW0F6D0';

  const copy = (e) => {
    e.stopPropagation();
    e.preventDefault();

    copyToClipboard(url);

    notification.success({
      message: 'Link copied to clipboard.',
    });
  };

  return (
    <AppLayout
      title={`Employees - ${currentRecord ? currentRecord.fields['Full Name'] : 'Create New'}`}
      breadcrumb={[
        {
          key: 'employees',
          label: 'Employees',
          href: '/employees',
        },
        {
          key: 'employee',
          label: currentRecord ? currentRecord.fields['Full Name'] : 'Create New',
          href: `/employees/${currentRecord ? currentRecord.id : 'new'}`,
        },
      ]}
      menuItemKey="employees"
      sideContent="test123"
    >
      <AirTableForm
        table="Employee"
        onSubmit={
          currentRecord
            ? AirTableFormHelpers.submitUpdateHandler(
              'Employee',
              {
                currentRecord,
                setCurrentRecord,
                callback: (record) => {

                },
              }
            )
            : AirTableFormHelpers.submitCreateHandler('Employee', {
              router,
              redirectUrl: '/employees/:id',
              callback: setCurrentRecord,
            })
        }
        existingRecord={currentRecord}
      >
        <Layout style={{ backgroundColor: 'transparent' }}>
          <Layout.Sider width={280} style={{ backgroundColor: 'transparent', paddingRight: '24px' }}>
            <Divider>
              {currentRecord?.fields?.['Full Name'] || 'New Employee'}
            </Divider>
            <AirTableField columnName="Job Title" />
            <AirTableField columnName="Department" />
            <AirTableField columnName="Entry Date" />
            <AirTableField columnName="Mobile Number" />
            <AirTableField columnName="Email" />
          </Layout.Sider>
          <Layout.Content style={{ backgroundColor: 'transparent' }}>
            <div style={{ marginBottom: 16, textAlign: 'center' }}>
              <a href={url} onClick={copy}>💡 Click this to get the link off Employee Information Form</a>
            </div>
            <Tabs defaultActiveKey="1">
              <Tabs.TabPane tab="Personal" key="1">
                <Divider orientation="left">Basic Information</Divider>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="First Name" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Middle Name" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Last Name" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Nickname" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Date of Birth" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Marital Status" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Gender" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Citizenship" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Nationality" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Language" />
                  </Col>
                </Row>
                <Divider orientation="left">Contact Information</Divider>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Email" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Mobile Number" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Home Number" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="House No & Street Name" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Village/Compound" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Barangay" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="City" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="State/Province" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Zip" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Country" />
                  </Col>
                  <Col span={16}>
                    <AirTableField columnName="Address 2" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Contact Person" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Contact Person Mobile No" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Relation to Contact Person" />
                  </Col>
                </Row>
              </Tabs.TabPane>
              <Tabs.TabPane tab="Employment" key="2">
                <Divider orientation="left">Employment Information</Divider>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Employee ID" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Account Status" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Employment Status" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Job Title" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Job Level" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Department" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Company" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Department Supervisor" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Direct Manager" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Workplace" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Entry Date" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Regularization Date" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Exit Date" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={24}>
                    <AirTableField columnName="Skills" />
                  </Col>
                </Row>
              </Tabs.TabPane>
              <Tabs.TabPane tab="Compensation & Benefits" key="3">
                <Divider orientation="left">Compensation &amp; Benefits</Divider>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Basic Pay" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="E-Cola" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Wage Zone" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Payroll Grouping" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Payment Method" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Pay Basis" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Bank" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Bank Account" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="HMO Number" />
                  </Col>
                </Row>
                <Divider orientation="left">ID Numbers</Divider>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="SSS Number" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Philhealth Number" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Pag-ibig Number" />
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Tax Identification Number" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Passport Number" />
                  </Col>
                </Row>
              </Tabs.TabPane>
              <Tabs.TabPane tab="Attendance & Leave" key="4">
                <Divider orientation="left">Attendance &amp; Leave Information</Divider>
                <Row gutter={24}>
                  <Col span={8}>
                    <AirTableField columnName="Days of Work Per Year" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Daily Work Hours" />
                  </Col>
                  <Col span={8}>
                    <AirTableField columnName="Leave Scheme" />
                  </Col>
                </Row>
              </Tabs.TabPane>
            </Tabs>
            <Errors />
            <Space size="middle">
              <SubmitButton>Submit</SubmitButton>
              <ResetButton>Reset</ResetButton>
            </Space>
          </Layout.Content>
        </Layout>
      </AirTableForm>
    </AppLayout>
  );
};

export default EmployeeForm;
