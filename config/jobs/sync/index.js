import departmentsSync from './departments';
import employeesSync from './employees';
import paydayEmployeesSync from './payday-employees';
import paydayMasterRecordsSync from './payday-master-record';

const paydayMasterRecords = async () => {
  await paydayMasterRecordsSync({
    paydayTable: 'wages',
    airtable: 'Wage Zones',
    map: {
      Code: 'REGION',
      Description: 'Description',
      Amount: 'Minimum Wage',
    },
  });


  await paydayMasterRecordsSync({
    paydayTable: 'jobs/status',
    airtable: 'Account Statuses',
    map: {
      Status: 'Name',
    }
  });

  await paydayMasterRecordsSync({
    paydayTable: 'groupings',
    airtable: 'Payroll Groupings',
    map: {
      Code: 'Name',
      Description: 'Description',
    }
  });

  await paydayMasterRecordsSync({
    paydayTable: 'companies',
    airtable: 'Companies',
    map: {
      Code: 'CODE',
      Name: 'Company Name',
      Address: 'Address',
      ZipCode: 'ZIP Code',
      ContactPerson: 'Contact Person',
      Position: 'Contact Position',
      FaxNo: 'Fax Number',
      MobileNo: 'Mobile Number',
      TelephoneNo: 'Telephone Number',
      Pagibig: 'PAGIBIG ID',
      PhilHealth: 'PHILHEALTH ID',
      SSS: 'SSS ID',
      TaxId: 'TIN NUMBER',
    }
  });

  await paydayMasterRecordsSync({
    paydayTable: 'nationalities',
    airtable: 'Nationalities',
    map: {
      Name: 'Name',
    }
  });

  await paydayMasterRecordsSync({
    paydayTable: 'days/per/year',
    airtable: 'Days of Work Per Year',
    map: {
      Name: 'Days',
    }
  });

  await paydayMasterRecordsSync({
    paydayTable: 'employment/status',
    airtable: 'Employment Statuses',
    map: {
      Status: 'Name',
    }
  });

  await paydayMasterRecordsSync({
    paydayTable: 'pay/basis',
    airtable: 'Pay Basis',
    map: {
      Basis: 'Name',
    }
  });

  await paydayMasterRecordsSync({
    paydayTable: 'locations',
    airtable: 'Workplaces',
    map: {
      Code: 'Code',
      Name: 'Workplace',
    }
  });
};

const runSync = async (name, callback, interval) => {
  while (true) {
    try {
      const start = Date.now();
      await callback();

      const remaining = interval - (Date.now() - start);
  
      if (remaining > 0) {
        console.log(`[${name}] Waiting ${Math.floor(remaining / 100) / 10}s...`);
        await new Promise((resolve) => setTimeout(resolve, remaining));
      }
    } catch (e) {
      console.error(`[${name}] Error syncing`, e);
    }
  }
};

const lemonadeSync = async () => {
  runSync('Department/Employee Sync', async () => {
    const allDepartments = await departmentsSync();
    await employeesSync({ allDepartments });
    await paydayEmployeesSync();
  }, 10000);

  runSync('PayDay Master Records Sync', paydayMasterRecords, 300000);
};

export default lemonadeSync;
