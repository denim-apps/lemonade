import Yup from 'yup';

export const movementTypes = {
  'job-position': {
    label: 'Job Position',
    table: 'Job Position Movement',
    columns: ['Job Position'],
  },
  'department': {
    label: 'Department',
    table: 'Department Movement',
    columns: ['Department'],
  },
  'basic-pay': {
    label: 'Basic Pay',
    table: 'Basic Pay Movement',
    columns: ['Basic Pay'],
  },
  'employment-status': {
    label: 'Employment Status',
    table: 'Employment Status Movement',
    columns: ['Employment Status'],
  },
  'allowance': {
    label: 'Allowance',
    table: 'Allowance Movement',
    columns: ['Allowance', 'Amount'],
  },
};

/**
 * @type {{
 *  [table: string]: {
 *    [column: string]: (validation: Yup.BaseSchema) => Yup.BaseSchema
 *  }
 * }}
 */
export default {
  'Table Name': {
    'Column Name': (validation) => {
      return validation.required(true).nullable(false);
    },
  },
  'Employee': {
    'Last Name': (validation) => {
      return validation.required(true).nullable(false);
    },
  },
  ...Object.keys(movementTypes).reduce((current, type) => {
    const movementType = movementTypes[type];

    return {
      ...current,
      [movementType.table]: movementType.columns.reduce((current, column) => ({
        ...current,
        [column]: (validation) => {
          return validation.required(true).nullable(false);
        },
      }), {
        'Effective Date': (validation) => {
          return validation.required(true).nullable(false);
        },
        'User ID': (validation) => {
          return validation.required(true).nullable(false).min(1);
        },
      }),
    };
  }, {}),
};
