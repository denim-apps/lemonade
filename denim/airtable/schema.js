import slugify from 'slugify';
import aliases from '../../config/table-aliases';

/**
 * @type {import('./schema-retriever').SchemaResult}
 */
const cache = require('../../.atcache.json');

export const __cache = cache;

/**
 * @param {string} identifier 
 * @returns {import('./schema-retriever').AirTable}
 */
export const findTableSchema = (identifier) => {
  const baseIds = Object.keys(cache.bases);

  for (let i = 0; i < baseIds.length; i++) {
    const baseId = baseIds[i];
    const base = cache.bases[baseId];

    for (let t = 0; t < base.tables.length; t++) {
      const table = base.tables[t];
      
      if (
        table.id === identifier
        || table.name.toLowerCase() === identifier.toLowerCase()
        || String(aliases[table.name] || slugify(table.name)).toLowerCase() === identifier.toLowerCase()
      ) {
        return table;
      }
    }
  }

  return null;
};

export const quoteValue = (value) => {
  return '"' + value.replace(/"/g, '""') + '"';
};
