import PaydayClient from '../../../connections/payday';
import { listRecords, updateRecords } from '../../../denim/airtable/data';
import diffValues from '../../../denim/utils/diff-values';
import syncLog from './sync-log';
import createSyncer from './syncer';

/**
 * @param {import('../../../denim/airtable/data').AirTableRecord} airtableRecord 
 * @param {string} key 
 */
const readChildPaydayId = (airtableRecord, key) => airtableRecord.children[key]?.[0]?.fields?.['PDY ID'];

/**
 * @param {import('../../../denim/airtable/data').AirTableRecord} airtableEmployee  
 */
const convertAirtableEmployee = (airtableEmployee) => {
  const employee = {
    employee_id: airtableEmployee.fields['Employee ID'],
    last_name: airtableEmployee.fields['Last Name'],
    first_name: airtableEmployee.fields['First Name'],
    middle_name: airtableEmployee.fields['Middle Name'],
    birthdate: airtableEmployee.fields['Date of Birth'],
    pagibig: airtableEmployee.fields['Pag-ibig Number'],
    phil_health: airtableEmployee.fields['Philhealth Number'],
    sss: airtableEmployee.fields['SSS Number'],
    tax_id: airtableEmployee.fields['Tax Identification Number'],
    company: readChildPaydayId(airtableEmployee, 'Company'),
    location: readChildPaydayId(airtableEmployee, 'Workplace'),
    wage: readChildPaydayId(airtableEmployee, 'Wage Zone'),
    employment_status: readChildPaydayId(airtableEmployee, 'Employment Status'),
    job_status: readChildPaydayId(airtableEmployee, 'Account Status'),
    days_per_year: readChildPaydayId(airtableEmployee, 'Days of Work Per Year'),
    pay_basis: readChildPaydayId(airtableEmployee, 'Pay Basis'),
    basic_rate: (airtableEmployee.fields['Basic Pay'] ? airtableEmployee.fields['Basic Pay'] : 0) * 12 / airtableEmployee.children['Days of Work Per Year']?.[0]?.fields['Name'],
    ecola: airtableEmployee.fields['E-Cola'],
    grouping: readChildPaydayId(airtableEmployee, 'Payroll Grouping'),
    payment_method: readChildPaydayId(airtableEmployee, 'Payment Method'),
    bank_account: airtableEmployee.fields['Bank Account'] || '',
    date_hired: airtableEmployee.fields['Entry Date'],
    start_date: airtableEmployee.fields['Entry Date'],
    regularization_date: airtableEmployee.fields['Regularization Date'],
    mobile_no: airtableEmployee.fields['Mobile Number'] || 'n/a',
    telephone_no: airtableEmployee.fields['Home Number'] || 'n/a',
    email: airtableEmployee.fields['Email'] || '',
    nationality: readChildPaydayId(airtableEmployee, 'Nationality'),
    sex: airtableEmployee.fields['Gender'],
    civil_status: airtableEmployee.fields['Marital Status'],
    monthly_rate: airtableEmployee.fields['Basic Pay'] ? airtableEmployee.fields['Basic Pay'] : 0,
    basic_adjustment: 0,
  };

  Object.keys(employee).forEach((key) => {
    if (employee[key] && ['birthdate', 'date_hired', 'start_date', 'regularization_date'].includes(key)) {
      employee[key] = `${employee[key]}T00:00:00`;
    }

    if (typeof(employee[key]) === 'undefined') {
      employee[key] = null;
    }
  });

  return employee;
};

const readToWriteMap = {
  Id: 'id',
  EmployeeId: 'employee_id',
  Lastname: 'last_name',
  Firstname: 'first_name',
  Middlename: 'middle_name',
  Birthdate: 'birthdate',
  Pagibig: 'pagibig',
  PhilHealth: 'phil_health',
  SSS: 'sss',
  TaxId: 'tax_id',
  Company: 'company',
  Location: 'location',
  Wage: 'wage',
  EmploymentStatus: 'employment_status',
  JobsStatus: 'job_status',
  DaysPerYear: 'days_per_year',
  Paybasis: 'pay_basis',
  MonthlyRate: 'monthly_rate',
  BasicRate: 'basic_rate',
  Ecola: 'ecola',
  BasicAdjustment: 'basic_adjustment',
  Grouping: 'grouping',
  PaymentMethod: 'payment_method',
  BankAccount: 'bank_account',
  DateHired: 'date_hired',
  StartDate: 'start_date',
  RegularizationDate: 'regularization_date',
  ResignationDate: 'resignation_date',
  MobileNo: 'mobile_no',
  TelephoneNo: 'telephone_no',
  Email: 'email',
  Nationality: 'nationality',
  Sex: 'sex',
  CivilStatus: 'civil_status',
  BasicRate: 'basic_rate',
};

const convertReadToWrite = (paydayEmployee) => {
  const map = Object.keys(paydayEmployee).reduce((current, key) => {
    return {
      ...current,
      [readToWriteMap[key]]: paydayEmployee[key],
    };
  }, {});
  
  Object.keys(map).forEach((key) => {
    if (map[key]?.Id) {
      map[key] = map[key].Id;
    }
  });

  return map;
};

const paydayEmployeesSync = createSyncer({
  name: 'PayDay Employee',
  init: async (options, context) => {
  },
  destinationRetriever: async (options, context) => {
    console.log('[PAYDAY SYNC] Retrieving employees from PayDay...');
    const paydayEmployees = await PaydayClient.getEmployees();

    return paydayEmployees;
  },
  sourceRetriever: async (options, context) => {
    console.log('[PAYDAY SYNC] Retrieving employees from AirTable...');

    const { records } = await listRecords('Employee', {
      all: true,
      expand: [
        'Nationality',
        'Account Status',
        'Payment Method',
        'Days of Work Per Year',
        'Payroll Grouping',
        'Employment Status',
        'Wage Zone',
        'Company',
        'Workplace',
        'Pay Basis',
      ],
    });

    return records;
  },
  sourceItemValid: async (airtableEmployee) => {
    const convertedEmployee = convertAirtableEmployee(airtableEmployee);
    const requiredFields = [
      'employee_id',
      'last_name',
      'first_name',
      'middle_name',
      'birthdate',
      'pagibig',
      'phil_health',
      'sss',
      'tax_id',
      'company',
      'location',
      'wage',
      'employment_status',
      'job_status',
      'days_per_year',
      'pay_basis',
      'monthly_rate',
      'basic_rate',
      'ecola',
      'basic_adjustment',
      'grouping',
      'payment_method',
      'bank_account',
      'date_hired',
      'start_date',
      'mobile_no',
      'telephone_no',
      'email',
      'nationality',
      'sex',
      'civil_status',
    ];

    const missingFields = [];

    for (let i = 0; i < requiredFields.length; i++) {
      if (convertedEmployee[requiredFields[i]] === null || typeof (convertedEmployee[requiredFields[i]]) === 'undefined') {
        missingFields.push(requiredFields[i]);
      }
    }

    if (missingFields.length) {
      console.log(`[PAYDAY SYNC] Validation failure for employee ${airtableEmployee.fields['User ID']}, missing fields: ${missingFields.join(', ')}`);
      return false;
    }

    return true;
  },
  findInDestination: (paydayEmployees, airtableEmployee) => {
    return paydayEmployees.find((employee) => employee['EmployeeId'] === airtableEmployee.fields['Employee ID']);
  },
  convertSourceToDestination: (airtableEmployee) => {
    return convertAirtableEmployee(airtableEmployee);
  },
  requiresUpdate: (convertedSourceItem, destinationItem) => {
    const difference = diffValues(convertReadToWrite(destinationItem), convertedSourceItem, true);

    if (Object.keys(difference).length > 0) {
      return true;
    }

    return false;
  },
  destinationUpdator: async (convertedSourceItem, destinationItem, airtableEmployee, options, context) => {
    console.log('[PAYDAY SYNC] Updating Payday Employee ' + airtableEmployee.fields['User ID']);

    // Update the existing employee.
    try {
      return await PaydayClient.updateEmployee(airtableEmployee.fields['Employee ID'], convertedSourceItem);
    } catch (e) {
      if (e.loggable) {
        let response = await (e.inner?.text?.() || 'n/a');
        const log = `${e.message}\nENDPOINT: ${
          e.endpoint
        }\n\nHEADERS:\n${Object.keys(e.headers)
          .map((header) => `${header}: ${e.headers[header]}`)
          .join('\n')}\n\nDATA: ${JSON.stringify(
          e.data
        )}\n\nRESPONSE: ${response}`;
        console.log(
          `[PAYDAY SYNC] EMPLOYEE SYNC ERROR: ${airtableEmployee.fields['Employee ID']}\n${log}`
        );

        await syncLog(airtableEmployee.id, 'LEMONADE-PAY-SYNC', log);
      } else {
        throw e;
      }
    }
  },
  destinationCreator: async (convertedSourceItem, airtableEmployee, options, context) => {
    console.log('[PAYDAY SYNC] Creating Payday Employee ' + airtableEmployee.fields['User ID']);

    // Create a new employee.
    try {
      return await PaydayClient.createEmployee(convertedSourceItem);
    } catch (e) {
      if (e.loggable) {
        let response = await (e.inner?.text?.() || 'n/a');
        const log = `${e.message}\nENDPOINT: ${
          e.endpoint
        }\n\nHEADERS:\n${Object.keys(e.headers)
          .map((header) => `${header}: ${e.headers[header]}`)
          .join('\n')}\n\nDATA: ${JSON.stringify(
          e.data
        )}\n\nRESPONSE: ${response}`;

        console.log(
          `[PAYDAY SYNC] EMPLOYEE SYNC ERROR: ${airtableEmployee.fields['Employee ID']}\n${log}`
        );

        await syncLog(airtableEmployee.id, 'LEMONADE-PAY-SYNC', log);
      } else {
        throw e;
      }
    }
  },
  associate: async (sourceItem, destinationItem) => {
    if (sourceItem.id) {
      if (sourceItem.fields['PDY ID'] != destinationItem?.Id) {
        await updateRecords('Employee', {
          id: sourceItem.id,
          fields: {
            'PDY ID': destinationItem.Id,
          },
        });
      }
    }
  },
});

export default paydayEmployeesSync;
