import got from 'got';
import QueryString from 'qs';
import * as yup from 'yup';
import { createRecordValidator, validateRecord } from '../validation/record-validation';
import { findTableSchema, __cache } from './schema';
import { createValidationShapeFromSchema } from './validation';

/**
 * @typedef {{ field: string, options: ListRecordsOptions }} FullExpansion
 */

/**
 * @typedef {string | string[] | FullExpansion | FullExpansion[]} Expansion
 */

/**
 * @typedef {{
 *  fields?: string[],
 *  filterByFormula?: string,
 *  maxRecords?: number,
 *  pageSize?: number,
 *  sort?: {
 *    field: string,
 *    direction?: 'asc' | 'desc'
 *  }[],
 *  view?: string,
 *  cellFormat?: 'string' | 'json',
 *  timeZone?: string,
 *  userLocale?: string,
 *  offset?: string,
 *  expand?: Expansion,
 *  all?: boolean,
 *  ids?: string[] | string,
 * }} ListRecordsOptions
 */

/**
 * @typedef {{
 *  expand?: Expansion,
 * }} RetrieveRecordOptions
 */

/**
 * @typedef {{
 *  ids?: string[] | string,
 *  filterByFormula?: string,
 * }} DeleteRecordsOptions
 */

/**
 * @typedef {{
 *  id: string,
 *  fields: {
 *    [key: string]: any,
 *  },
 *  createdTime: string,
 *  children?: {
 *    [key: string]: AirTableRecord[]
 *  },
 * }} AirTableRecord
 * @global
 */

/**
 * Expands records.
 * 
 * @param {FullExpansion[]} expansion 
 * @param {import('./schema-retriever').AirTable} tableSchema 
 * @param {AirTableRecord[]} records 
 */
export const expandRecords = async (expansion, tableSchema, records, executingUser) => {
  try {
    if (expansion && expansion.length) {
      let relationships, childExpansions, rootExpansions;
      relationships = {};

      childExpansions = expansion.reduce(
        (current, next) => {
          const [root, field] = next.field.split('.', 2);

          return {
            ...current,
            [root]: [
              ...(current[root] || []),
              ...(field ? [{ field, options: next.options }] : []),
            ],
          };
        },
        {}
      );

      rootExpansions = Object.keys(childExpansions);

      for (let i = 0; i < rootExpansions.length; i++) {
        const rootExpansion = rootExpansions[i];
        const options = expansion.find(({ field }) => field === rootExpansion)?.options;
        const columnSchema = tableSchema.columns.find(({ name }) => name === rootExpansion);

        if (columnSchema && columnSchema.type === 'foreignKey') {
          const childExpansion = childExpansions[rootExpansion] || [];

          // Collect records.
          const ids = [];

          records.forEach((record) => {
            ids.push(...(record.fields[columnSchema.name] || []));
          });

          const { records: childRecords } = await listRecords(columnSchema.typeOptions.foreignTableId, {
            ...options,
            expand: childExpansion,
            user: executingUser,
            ids,
            all: true,
          });

          records.forEach((record) => {
            const children = (record.fields[columnSchema.name] || []).map((id) => {
              return childRecords.find((record) => record.id === id);
            }).filter(Boolean);

            record.children = record.children || {};
            record.children[columnSchema.name] = children;
          });
        }
      }
    }
  } catch (e) {
    if (e.response && e.response.body) {
      throw new Error(e.response.body);
    } else {
      throw e;
    }
  }
};

/**
 * @param {string | string[] | FullExpansion | FullExpansion[]} expansion 
 * @returns {FullExpansion[]}
 */
const realizeExpansion = (expansion) => {
  if (!expansion) {
    return [];
  }

  /**
   * @type {FullExpansion[]}
   */
  let fullExpansion = expansion;

  if (!Array.isArray(expansion)) {
    fullExpansion = [expansion];
  }

  for (let i = 0; i < fullExpansion.length; i++) {
    if (typeof (fullExpansion[i]) === 'string') {
      fullExpansion[i] = {
        field: fullExpansion[i],
        options: {},
      };
    }
  }

  return fullExpansion;
};

/**
 * @param {string} table 
 * @param {ListRecordsOptions} [options]
 * @returns {Promise<{
 *  records: AirTableRecord[],
 *  offset?: string,
 * }>}
 */
export const listRecords = async (
  table,
  options = {},
) => {
  try {
    const tableSchema = findTableSchema(table);
    const result = { records: [], offset: options.offset };
    const expansion = options.expand;
    delete options.expand;

    if (options.all || options.ids) {
      delete options.pageSize;
    }

    if (options.ids) {
      // Hotfix/workaround for the array not always being passed correctly.
      if (typeof (options.ids) === 'object' && !Array.isArray(options.ids)) {
        options.ids = Object.values(options.ids);
      }

      const idsFilter = 'OR(' + options.ids.map((id) => 'RECORD_ID()="' + id + '"').join(', ') + ')';
      options.filterByFormula = options.filterByFormula ? ('AND(' + idsFilter + ', ' + options.filterByFormula + ')') : idsFilter;
      delete options.ids;
    }

    const executingUser = options.user;
    delete options.user;

    while (true) {
      if (result.offset) {
        options.offset = result.offset;
      }

      const response = await got(`https://api.airtable.com/v0/${tableSchema.baseId}/${tableSchema.name}`, {
        headers: {
          Authorization: 'Bearer ' + __cache.apiKey,
        },
        searchParams: new URLSearchParams(QueryString.stringify(options)),
      }).json();

      result.records.push(...response.records);
      result.offset = response.offset;

      if (!result.offset || !options.all) {
        break;
      }
    }

    // Expansions.
    await expandRecords(realizeExpansion(expansion), tableSchema, result.records, executingUser);

    return result;
  } catch (e) {
    if (e.response && e.response.body) {
      throw new Error(e.response.body);
    } else {
      throw e;
    }
  }
};

/**
 * @param {string} table 
 * @param {string} id 
 * @param {RetrieveRecordOptions} [options] 
 * @returns {Promise<AirTableRecord>}
 */
export const retrieveRecord = async (
  table,
  id,
  options = {},
) => {
  try {
    const tableSchema = findTableSchema(table);

    const response = await got(`https://api.airtable.com/v0/${tableSchema.baseId}/${tableSchema.name}/${id}`, {
      headers: {
        Authorization: 'Bearer ' + __cache.apiKey,
      },
    }).json();

    const expansion = options?.expand || [];

    await expandRecords(realizeExpansion(expansion), tableSchema, [response], options.user);

    return response;
  } catch (e) {
    if (e.response && e.response.body) {
      throw new Error(e.response.body);
    } else {
      throw e;
    }
  }
};

/**
 * @param {import('./schema-retriever').AirTable} tableSchema 
 * @param {boolean} isUpdate
 */
const createValidator = (tableSchema, isUpdate) => {
  const shape = {
    fields: createRecordValidator(createValidationShapeFromSchema(tableSchema), isUpdate),
    children: yup.lazy((value) => {
      const shape = Object.keys(value || {}).reduce((current, relationshipKey) => {
        const columnSchema = tableSchema.columns.find(({ name }) => name === relationshipKey);

        if (columnSchema) {
          return {
            ...current,
            [relationshipKey]: yup.lazy((value) => {
              const tableSchema = findTableSchema(columnSchema.typeOptions.foreignTableId);

              return createValidator(tableSchema, !!value.id);
            }),
          };
        }

        return current;
      }, {});

      return yup.object().shape(shape).nullable(true);
    }),
  };

  if (isUpdate) {
    shape.id = yup.string().nullable(!isUpdate).required(isUpdate);
  }

  return yup.object().shape(shape);
};

/**
 * @template {AirTableRecord | AirTableRecord[]} T
 * @param {string} table 
 * @param {T} recordsInput 
 * @param {boolean} isCreate 
 * @param {import('../lark/api').LarkUser} executingUser
 * @returns {Promise<T>}
 */
export const updateRecords = async (
  table,
  recordsInput,
  isCreate,
  executingUser,
) => {
  try {
    const tableSchema = findTableSchema(table);
    let records = Array.isArray(recordsInput) ? recordsInput : [recordsInput];

    const recordSchema = createValidator(tableSchema, !isCreate);
    const { valid, record, errors } = validateRecord(yup.array(recordSchema), records.map((record) => {
      return {
        ...record,
        fields: Object.keys(record.fields).reduce((current, field) => {
          const columnSchema = tableSchema.columns.find(({ name }) => name === field);

          if (columnSchema && columnSchema.type === 'foreignKey' && typeof (record.fields[field]) === 'string') {
            return {
              ...current,
              [field]: [record.fields[field]],
            };
          }

          return {
            ...current,
            [field]: record.fields[field],
          };
        }, {}),
      };
    }));

    if (!valid) {
      const error = new Error('Record validation error.');
      error.isValidationError = true;
      error.validationErrors = errors;

      throw error;
    }

    records = record;

    // Gather other requests to perform.
    const additionalRequests = [];
    const allRelationships = [];

    records.forEach((record, i) => {
      if (record.children) {
        Object.keys(record.children).forEach((key) => {
          record.children[key].forEach((child, c) => {
            additionalRequests.push({
              type: child.id ? 'UPDATE' : 'CREATE',
              record: child,
              recordIndex: i,
              relationship: key,
              childIndex: c,
            });
          });

          if (!allRelationships.includes(key)) {
            allRelationships.push(key);
          }
        });
      }
    });

    // Perform creates/updates.
    await Promise.all(allRelationships.map(async (relationshipKey) => {
      const columnSchema = tableSchema.columns.find(({ name }) => name === relationshipKey);

      if (columnSchema) {
        const createRequests = additionalRequests.filter(({ type, relationship }) => type === 'CREATE' && relationship === relationshipKey);
        const updateRequests = additionalRequests.filter(({ type, relationship }) => type === 'UPDATE' && relationship === relationshipKey);
        const buckets = [];

        // Bucket the create requests.
        for (let i = 0; i < createRequests.length; i += 10) {
          buckets.push(createRequests.slice(i, i + 10));
        }

        // Bucket the update requests.
        for (let i = 0; i < updateRequests.length; i += 10) {
          buckets.push(updateRequests.slice(i, i + 10));
        }

        await Promise.all(buckets.map(async (bucket) => {
          const results = await updateRecords(columnSchema.typeOptions.foreignTableId, bucket.map(({ record }) => record), bucket[0].type === 'CREATE', executingUser);

          for (let i = 0; i < bucket.length; i++) {
            const request = bucket[i];
            const record = results[i];
            records[request.recordIndex].children[relationshipKey][request.childIndex] = {
              ...record,
              ...records[request.recordIndex].children[relationshipKey][request.childIndex],
            };
          }
        }));
      }
    }));

    if (!records.length) {
      return [];
    }

    const response = await got(`https://api.airtable.com/v0/${tableSchema.baseId}/${tableSchema.name}`, {
      method: isCreate ? 'POST' : 'PATCH',
      json: {
        records: records.map((record) => {
          const fields = record.fields;

          // Add any already existing IDs from the children.
          Object.keys(record.children || []).forEach((key) => {
            const children = record.children[key];

            children.forEach(({ id }) => {
              if (id) {
                if (!fields[key]) {
                  fields[key] = [];
                }

                if (!fields[key].includes(id)) {
                  fields[key].push(id);
                }
              }
            });
          });

          return {
            id: record.id,
            fields,
          };
        }),
      },
      headers: {
        Authorization: 'Bearer ' + __cache.apiKey,
      },
    }).json();

    response.records.forEach((record, i) => {
      record.children = records[i].children;
    });

    if (Array.isArray(recordsInput)) {
      return response.records;
    } else {
      return response.records[0];
    }
  } catch (e) {
    if (e.response && e.response.body) {
      throw new Error(e.response.body);
    } else {
      throw e;
    }
  }
};

/**
 * @param {string} table 
 * @param {DeleteRecordsOptions} options 
 */
export const deleteRecords = async (table, options) => {
  try {
    const tableSchema = findTableSchema(table);
    const ids = options.ids || [];
    let filterFormula = '';

    if (options.filterByFormula) {
      filterFormula = options.filterByFormula;

      if (ids.length) {
        filterFormula = `AND(${filterFormula}, ${'OR(' + options.ids.map((id) => 'RECORD_ID()="' + id + '"').join(', ') + ')'})`;
      }
    }

    const deletedRecords = [];

    const batchDeleteRecords = async (batch) => {
      const searchParams = new URLSearchParams(batch.map((id) => ['records[]', id]));

      return await got(`https://api.airtable.com/v0/${tableSchema.baseId}/${tableSchema.name}`, {
        method: 'DELETE',
        searchParams,
        headers: {
          Authorization: 'Bearer ' + __cache.apiKey,
        },
      }).json();
    };

    if (filterFormula) {
      let offset = undefined;

      while (true) {
        const result = await got(`https://api.airtable.com/v0/${tableSchema.baseId}/${tableSchema.name}`, {
          headers: {
            Authorization: 'Bearer ' + __cache.apiKey,
          },
          searchParams: {
            filterByFormula: filterFormula,
            offset,
            pageSize: 10,
          },
        }).json();

        offset = result.offset;

        const ids = result.records.map(({ id }) => id);
        const response = await batchDeleteRecords(ids);

        deletedRecords.push(...response.records);

        if (!result.offset) {
          break;
        }
      }
    } else {
      // Batch 10s.
      const idBatches = [];

      for (let i = 0; i < ids.length; i += 10) {
        idBatches.push(ids.slice(i, i + 10));
      }

      for (let i = 0; i < idBatches.length; i++) {
        const batch = idBatches[i];
        const response = await batchDeleteRecords(batch);

        deletedRecords.push(...response.records);
      }
    }

    return deletedRecords;
  } catch (e) {
    if (e.response && e.response.body) {
      throw new Error(e.response.body);
    } else {
      throw e;
    }
  }
};
