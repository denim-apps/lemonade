import got from 'got';
import { listRecords, updateRecords } from '../../../denim/airtable/data';
import LarkApi from '../../../denim/lark/api';
import diffValues from '../../../denim/utils/diff-values';

const departmentsSync = async () => {
  console.log('[LARK DEPARTMENT SYNC] Retrieving departments from Lark...');
  const larkAccessToken = await LarkApi.getAppAccessToken();
  const larkDepartments = [];
  const allDepartments = [];
  let pageToken = -1;

  while (pageToken) {
    const departmentsPage = await got(`https://open.larksuite.com/open-apis/contact/v3/departments?parent_department_id=0&${pageToken !== -1 ? `&page_token=${pageToken}` : ''}`, {
      headers: {
        Authorization: 'Bearer ' + larkAccessToken,
      },
    }).json();

    if (departmentsPage.data.items) {
      larkDepartments.push(...departmentsPage.data.items);
    }

    pageToken = departmentsPage.data.page_token;
  }

  console.log('[LARK DEPARTMENT SYNC] Retrieving departments from AirTable...');

  const { records } = await listRecords('Department', {
    all: true,
  });

  for (let i = 0; i < records.length; i++) {
    const airtableDepartment = records[i];
    let larkDepartment = null;

    try {
      // Ensure the department can already be created.
      if (airtableDepartment.fields['Department']) {
        // Check if this department exists in Lark.
        if (airtableDepartment.fields['Lark ID']) {
          const departmentById = larkDepartments.find(({ department_id }) => department_id === airtableDepartment.fields['Lark ID']);

          if (departmentById) {
            larkDepartment = departmentById;
          }
        }

        // Fallback to finding the department by name.
        if (!larkDepartment) {
          larkDepartment = larkDepartments.find(({ name }) => name === airtableDepartment.fields['Department']);
        }

        // Convert the data from AirTable.
        const convertedAirtableDepartment = {
          name: airtableDepartment.fields['Department'],
        };

        if (larkDepartment) {
          const difference = diffValues(larkDepartment, convertedAirtableDepartment);

          if (Object.keys(difference).length) {
            // Update the existing department.
            const response = await got(`https://open.larksuite.com/open-apis/contact/v3/departments/${airtableDepartment.fields['Lark ID']}?department_id_type=department_id`, {
              method: 'PATCH',
              headers: {
                Authorization: 'Bearer ' + larkAccessToken,
              },
              json: difference,
            }).json();

            if (response.data.department) {
              larkDepartment = response.data.department;
              console.log('[LARK DEPARTMENT SYNC] Updated Lark department ' + response.data.department.department_id);
            }
          }
        } else {
          // Create a new department.
          const response = await got(`https://open.larksuite.com/open-apis/contact/v3/departments`, {
            method: 'POST',
            headers: {
              Authorization: 'Bearer ' + larkAccessToken,
            },
            json: {
              ...convertedAirtableDepartment,
              parent_department_id: 0,
            },
          }).json();

          if (response.data.department) {
            larkDepartment = response.data.department;
            console.log('[LARK DEPARTMENT SYNC] Created Lark department ' + response.data.department.department_id);
          }
        }

        // Update the Lark ID if necessary.
        if (larkDepartment.department_id !== airtableDepartment.fields['Lark ID']) {
          await updateRecords('Department', {
            id: airtableDepartment.id,
            fields: {
              'Lark ID': larkDepartment.department_id,
            },
          });
        }
      }

      if (larkDepartment) {
        allDepartments.push(larkDepartment);
      }
    } catch (e) {
      console.error('[LARK DEPARTMENT SYNC] Department sync failure', airtableDepartment, larkDepartment, e.response?.body || e);
    }
  }

  return allDepartments;
};

export default departmentsSync;
