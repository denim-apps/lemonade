export default {
  'Table Name': {
    'retrieve': '/my/retrieve/endpoint',
    'list': '/my/list/endpoint',
    'update': '/my/update/endpoint',
    'destroy': '/my/destroy/endpoint',
  },
};
