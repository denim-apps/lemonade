/**
 * Converts a record from the dummy Users table into a Lark User.
 * 
 * @param {import("../denim/airtable/data").AirTableRecord} userRecord 
 * @returns {import("../denim/lark/api").LarkUser}
 */
const DummyAuth = async (userRecord) => {
  return {
    open_id: userRecord.id,
  };
};

export default DummyAuth;
