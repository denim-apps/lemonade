import moment from 'moment';
import larkAdminApi from '../../../lark-admin/admin-api';

/**
 * @param {string} query 
 * @returns {Promise<{
 *  Avatar: string;
 *  Dept: string;
 *  DeptId: string;
 *  Email: string;
 *  EmployeeId: string;
 *  EnName: string;
 *  IsTenantManager: boolean;
 * }[]>}
 */
export const retrieveAttendanceUsers = async (query = '') => {
  const {
    Body: {
      Users
    },
  } = await larkAdminApi.attendance.getUser(query);

  return Users;
};

/**
 * @param {Date} startDate 
 * @param {Date} endDate 
 * @param {string[]} userIds 
 * @returns 
 */
export const retrieveAttendance = async (startDate = new Date(Date.now() - (2 * 24 * 60 * 60 * 1000)), endDate = new Date(), userIds = []) => {
  const { ColumnFamilies: cf } = await larkAdminApi.attendance.getColumns();

  const queryBody = {
    "Body": {
      "CountPerBatch": 10000,
      "BatchNum": 0,
      "Query": {
        "TaskType": "daily",
        "StartDate": moment(startDate).format('YYYYMMDD'),
        "EndDate": moment(endDate).format('YYYYMMDD'),
        "GrpIds": [],
        "Uids": userIds,
        "NeedHistory": true
      },
      "ColumnFamilies": cf.map((cf) => ({
        ...cf,
        Columns: cf.Columns.map((column) => ({
          ...column,
          Value: '1',
        })),
        isAllChoose: true,
      })),
    },
    "Head": {}
  };

  const { Lists, ColumnFamilies } = await larkAdminApi.attendance.getStatistics(queryBody);

  return Lists.map((list) => ({
    ...list,
    __meta: { ColumnFamilies },
  }));
};

const retrieveOvertimeRules = async () => {
  const { ruleDTO } = await larkAdminApi.leaves.getOvertimeRules();
  const ruleList = [];

  for (let i = 0; i < ruleDTO.length; i++) {
    const rule = await larkAdminApi.leaves.getOvertimeRule(ruleDTO[i].id);
    ruleList.push(rule);
  }

  return ruleList;
};

export const retrieveHolidayTypesByDate = async () => {
  const overtimeRules = await retrieveOvertimeRules();

  const holidays = overtimeRules
    .reduce((current, rule) => {
      return current.concat(
        ...rule.subRule.map((subRule) => {
          const { subRule: subRules, ...parentRule } = rule;

          return {
            ...subRule,
            parentRule,
          };
        })
      );
    }, [])
    .reduce((current, subRule) => {
      return current.concat(
        ...subRule.specialDate.map((specialDate) => {
          const { specialDate: specialDates, parentRule } = subRule;

          return {
            ...specialDate,
            parentRule,
          };
        })
      );
    }, [])
    .reduce((current, rule) => {
      return current.concat({
        ...rule,
        id: rule.name,
      });
    }, []);

  const holidayTypesByDate = {};

  holidays.forEach((record) => {
    const holiday = {
      'Holiday Name': record.name,
      Date: record.date[0],
      'End Date': record.date[1],
      'Holiday Type': record.name.indexOf('SWH') === 0 || record.name.indexOf('SNH') === 0
        ? 'Special'
        : 'Legal',
    };

    const dates = [String(holiday.Date)];

    if (holiday['End Date']) {
      for (let date = moment(String(holiday.Date)).add(1, 'day'); date.isSameOrBefore(String(holiday['End Date'])); date = date.add(1, 'day')) {
        dates.push(date.format('YYYY-MM-DD'));
      }
    }

    dates.forEach((date) => {
      holidayTypesByDate[date] = String(holiday['Holiday Type']);
    });
  });

  return holidayTypesByDate;
};
