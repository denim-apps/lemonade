const diffValues = (existingRecord, newValues, ignoreUndefined = false) => {
  const updated = {};

  Object.keys(newValues).forEach((key) => {
    let compareA = newValues[key];
    let compareB = existingRecord[key];

    if (Array.isArray(compareA)) {
      compareA = compareA.join('|||');
    }

    if (Array.isArray(compareB)) {
      compareB = compareB.join('|||');
    }

    if (compareA !== compareB && (!ignoreUndefined || typeof(newValues[key]) !== 'undefined')) {
      updated[key] = newValues[key];
    }
  });

  return updated;
};

export default diffValues;
