export default {
  appId: process.env.LARK_APP_ID,
  appSecret: process.env.LARK_APP_SECRET,
};
