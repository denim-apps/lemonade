import QueryString from 'qs';
import slugify from 'slugify';
import secureEndpoints from '../../config/secure-endpoints';
import tableAliases from '../../config/table-aliases';

/**
 * @param {string} table 
 */
const getTableSlug = (table) => {
  if (tableAliases[table]) {
    return tableAliases[table];
  }

  return slugify(table);
};

const getTableEndpoint = (table, type) => {
  if (secureEndpoints[table]?.[type]) {
    return secureEndpoints[table][type];
  }

  return `/api/data/${getTableSlug(table)}`;
};

const AirTableClient = {
  /**
   * @param {string} table 
   * @param {import("./data").AirTableRecord | import("./data").AirTableRecord[]} records 
   * @returns {import("./data").AirTableRecord | import("./data").AirTableRecord[]}
   */
  create: async (table, records) => {
    const result = await fetch(
      `${getTableEndpoint(table, 'update')}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(records),
      },
    );

    return await result.json();
  },
  /**
   * @param {string} table 
   * @param {import("./data").AirTableRecord | import("./data").AirTableRecord[]} records 
   * @returns {import("./data").AirTableRecord | import("./data").AirTableRecord[]}
   */
  update: async (table, records) => {
    const result = await fetch(
      `${getTableEndpoint(table, 'update')}`,
      {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(records),
      },
    );

    return await result.json();
  },
  /**
   * @param {string} table 
   * @param {import("./data").DeleteRecordsOptions} options 
   * @returns {{ deleted: boolean, id: string }[]}
   */
  destroy: async (table, options) => {
    const result = await fetch(
      `${getTableEndpoint(table, 'destroy')}`,
      {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(options),
      },
    );

    return await result.json();
  },
  getTableSlug,
  getTableEndpoint,
  /**
   * Compares values and returns only those that have changed.
   * 
   * @param {import('./data').AirTableRecord} existingRecord 
   * @param {any} newValues 
   */
  diffValues: (existingRecord, newValues) => {
    const updated = {};

    Object.keys(newValues).forEach((key) => {
      let compareA = newValues[key];
      let compareB = existingRecord.fields[key];

      if (Array.isArray(compareA)) {
        compareA = compareA.join('|||');
      }

      if (Array.isArray(compareB)) {
        compareB = compareB.join('|||');
      }

      if ((typeof(compareA) === 'object' && !Array.isArray(compareA)) || (typeof(compareB) === 'object' && !Array.isArray(compareB))) {
        return;
      }

      if (compareA !== compareB) {
        updated[key] = newValues[key];
      }
    });

    return updated;
  },
  /**
   * @param {string} table 
   * @param {import("./data").ListRecordsOptions} options 
   * @returns {Promise<{
   *  records: import("./data").AirTableRecord[],
   *  offset?: string,
   * }>}
   */
  list: async (table, options) => {
    const result = await fetch(
      `${getTableEndpoint(table, 'list')}${options ? `?${QueryString.stringify(options)}` : ''}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );

    return await result.json();
  },
};

export default AirTableClient;
